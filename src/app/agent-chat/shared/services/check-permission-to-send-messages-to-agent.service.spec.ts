import { TestBed, inject } from '@angular/core/testing';

import { CheckPermissionToSendMessagesToAgentService } from './check-permission-to-send-messages-to-agent.service';

describe('CheckPermissionToSendMessagesToAgentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckPermissionToSendMessagesToAgentService]
    });
  });

  it('should be created', inject([CheckPermissionToSendMessagesToAgentService], (service: CheckPermissionToSendMessagesToAgentService) => {
    expect(service).toBeTruthy();
  }));
});
