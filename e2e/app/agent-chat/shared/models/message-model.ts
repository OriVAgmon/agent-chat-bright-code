export interface Message{
  messageId?:number;
  senderId:number;
  receiverId:number;
  sessionId:number;
  content:string;
  contentLanguage:string;
  DateTime:Date;
  isSenderAgent:boolean;
  isAgentToAgentMessage?:boolean;
  shouldTranslate?:boolean;
}