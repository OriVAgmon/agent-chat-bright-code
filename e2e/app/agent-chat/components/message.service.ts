import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';
  
@Injectable()
export class MessageService {

  constructor(private http: Http) { 
    
  }
  public send_message(sessionID: number, content: string, senderID: number, receiverID:number, is_sender_agent: boolean, content_language: string, isAgentToAgentMessage: boolean)
  {
    content = this.encodingSpecialChars(content);
    var uri = 'http://localhost:62752/api/messages/add/'+sessionID+'/'+senderID+'/'+receiverID+'/'+content+'/'+content_language+'/'+is_sender_agent+'/'+isAgentToAgentMessage;
    return this.http.get(uri)  
    .map((res: Response) => {return res.json() }); 
  }
  private encodingSpecialChars(orginalString)
  {
    orginalString = this.replaceAll(".", "***dotEncoding***", orginalString);
    orginalString = this.replaceAll("?", "***questionMarkEncoding***", orginalString);    
    orginalString = this.replaceAll("%3C", "***leftTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("%3E", "***rightTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("%2B", "***plusEncoding***", orginalString);    
    orginalString = this.replaceAll("%2F", "***slashEncoding***", orginalString);
    orginalString = this.replaceAll("%5C", "***backSlashEncoding***", orginalString);
    orginalString = this.replaceAll("%0A", "***newLineEncoding***", orginalString);
    return orginalString;
  }
  private replaceAll(src:string, dst:string, stringToOperateOn:string)
  {
    while(stringToOperateOn.indexOf(src) != -1)
    {
      stringToOperateOn = stringToOperateOn.replace(src, dst);
    }    
    return stringToOperateOn;
  }


}
