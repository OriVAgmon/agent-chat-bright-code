import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftChatAgentsComponent } from './left-chat-agents.component';

describe('LeftChatAgentsComponent', () => {
  let component: LeftChatAgentsComponent;
  let fixture: ComponentFixture<LeftChatAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftChatAgentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftChatAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
