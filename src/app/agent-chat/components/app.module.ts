import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';  
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FileUploadModule} from 'primeng/primeng';
import { HttpClient } from '@angular/common/http';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';



import * as $ from 'jquery';

import {
 
} from '@angular/material';


import { AppComponent } from './app.component';
import { LeftChatComponent } from './live-chat/left-chat/left-chat.component';
import { MiddleChatComponent } from './live-chat/middle-chat/middle-chat.component';
import { RightChatComponent } from './live-chat/right-chat/right-chat.component';
import { LeftChatAgentsComponent } from './live-chat/agents-chat/left-chat-agents/left-chat-agents.component';
import { MiddleChatAgentsComponent } from './live-chat/agents-chat/middle-chat-agents/middle-chat-agents.component';
import { RightChatAgentsComponent } from './live-chat/agents-chat/right-chat-agents/right-chat-agents.component';
import { ChatHistoryTabComponent } from './chat-history/chat-history-tab/chat-history-tab.component';
import { FilterHistoryComponent } from './chat-history/filter-history/filter-history.component';
import { ChatSessionsComponent } from './chat-history/chat-sessions/chat-sessions.component';
import { CannedMessagesComponent } from './canned-messages/canned-messages/canned-messages.component';
import { FilterMessagesComponent } from './canned-messages/filter-messages/filter-messages.component';
import { SearchMessagesComponent } from './canned-messages/search-messages/search-messages.component';

import { MessageService } from './../shared/services/message.service';
import { PassChosenChatIdService } from './chat-history/pass-chosen-chat-id.service';
import { GetAllUsersService } from './../shared/services/get-all-users.service';
import { GetAgentIdService } from './../shared/services/get-agent-id.service';
import { GetUserIdService } from './../shared/services/get-user-id.service';
import { GlobalVariblesService } from './../shared/services/global-varibles.service';
import { GetChatsHistoryService } from './../shared/services/get-chats-history.service';
import { SingalRService } from './../shared/services/singal-r.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';





@NgModule({
  declarations: [
    AppComponent,
    LeftChatComponent,
    MiddleChatComponent,
    RightChatComponent,
    LeftChatAgentsComponent,
    MiddleChatAgentsComponent,
    RightChatAgentsComponent,
    ChatHistoryTabComponent,
    FilterHistoryComponent,
    ChatSessionsComponent,
    CannedMessagesComponent,
    FilterMessagesComponent,
    SearchMessagesComponent,
  ],
  imports: [
    TooltipModule.forRoot(),
	BsDatepickerModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    HttpModule,
	BsDropdownModule.forRoot(),
    HttpClientModule
  ],
  exports: [
          ],
  providers: [MessageService, PassChosenChatIdService, GetAllUsersService, GetAgentIdService, GetUserIdService, GlobalVariblesService, GetChatsHistoryService, SingalRService],
  bootstrap: [AppComponent],
})
export class AppModule { }
