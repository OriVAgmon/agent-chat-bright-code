import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleChatAgentsComponent } from './middle-chat-agents.component';

describe('MiddleChatAgentsComponent', () => {
  let component: MiddleChatAgentsComponent;
  let fixture: ComponentFixture<MiddleChatAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleChatAgentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleChatAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
