
export interface Agent {
  email:string;
  FirstName:string;
  LastName:string;
  agentId:number;
  agentLocation:string;
  agentIp:string;
  websiteHistory:string;
  agentAvailability:string;
}
