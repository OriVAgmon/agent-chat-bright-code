import { TestBed, inject } from '@angular/core/testing';

import { GetAgentIdService } from './get-agent-id.service';

describe('GetAgentIdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetAgentIdService]
    });
  });

  it('should be created', inject([GetAgentIdService], (service: GetAgentIdService) => {
    expect(service).toBeTruthy();
  }));
});
