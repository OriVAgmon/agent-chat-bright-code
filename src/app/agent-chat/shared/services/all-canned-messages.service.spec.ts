import { TestBed, inject } from '@angular/core/testing';

import { AllCannedMessagesService } from './all-canned-messages.service';

describe('AllCannedMessagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllCannedMessagesService]
    });
  });

  it('should be created', inject([AllCannedMessagesService], (service: AllCannedMessagesService) => {
    expect(service).toBeTruthy();
  }));
});
