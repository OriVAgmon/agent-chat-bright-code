import { Component, Input, OnChanges, SimpleChanges, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from './../../../../shared/services/message.service';
import { AllCannedMessagesService } from '../../../../shared/services/all-canned-messages.service';
import { GetChatsHistoryService } from '../../../../shared/services/get-chats-history.service';
import { EnterConversationService } from '../../../../shared/services/enter-conversation.service';
import { GlobalVariblesService } from './../../../../shared/services/global-varibles.service';
import { SingalRService } from '../../../../shared/services/singal-r.service';
import { TranslateService } from './../../../../shared/services/translate.service';

import { Message } from './../../../../shared/models/message-model';
import { Agent } from '../../../../shared/models/agent-model';
import { CannedMessage } from '../../../../shared/models/canned-message-model';
import { User } from '../../../../shared/models/user-model';


@Component({
  selector: 'app-middle-chat-agents',
  templateUrl: './middle-chat-agents.component.html',
  styleUrls: ['./middle-chat-agents.component.scss'],
  providers: [AllCannedMessagesService,
    GetChatsHistoryService,
    EnterConversationService,
    TranslateService]
})
export class MiddleChatAgentsComponent implements OnInit {

  //Main varibles for this component:
  @Input() chosenUser:Agent;
  agentImg = "assets/profile_teaser.png";
  userImg = "https://d30y9cdsu7xlg0.cloudfront.net/png/468442-200.png";
  allMessages = [];
  cannedMessages: CannedMessage[];
  hasMessages = false;
  inputMessage:string;
  translateMyMessages: boolean;
  translateUserMessages: boolean;
  currChatID: number;
  agentID: number;
  shouldScrollDown = false;
  quickReply = false;
  fileUpload = false;
  currMessageContent: string;

  constructor(
    private message: MessageService,
    private allCannedMessages: AllCannedMessagesService,
    private specificChatHistory: GetChatsHistoryService,
    private enterConversationService: EnterConversationService,
    private signalRService: SingalRService,
    private globalVaribles: GlobalVariblesService,
    private translateService: TranslateService) {
    this.allCannedMessages.getAllCannedMessagesOfAgentById(this.globalVaribles.ourAgentID).subscribe(cannedMessages => {
      this.cannedMessages = cannedMessages;
    });
    this.agentID = this.globalVaribles.ourAgentID;
  }

  ngOnInit() {
    //will get the latest message from signalr service in real time:
    this.signalRService.observableLatestMessage.subscribe(m => this.recMessage(m));
    this.scrollDown();
  }
  uploadFile() {
    console.log("todo: upload file");
  }
  // This function receives a message:
  // (1) Will decode the message.
  // (2) Will translate it (if needed).
  // (3) Will push the new message to the message array ("allMessages").
  recMessage(m) {
    if (m != null && m.senderID == this.chosenUser.agentId) {
      m.content = decodeURIComponent(m.content);

      m.content = this.decodingSpecialChars(m.content);
      if (this.translateUserMessages) {
        this.translateService.translate(m.content).subscribe(res => this.handleTranslatedOutPut(res, false, m));
      }
      else {
        this.allMessages.push(m);
      }
      this.hasMessages = true;
      this.scrollDown();
    }
  }
  private replaceAll(src: string, dst: string, stringToOperateOn: string) {
    while (stringToOperateOn.indexOf(src) != -1) {
      stringToOperateOn = stringToOperateOn.replace(src, dst);
    }
    return stringToOperateOn;
  }
  //This function is the callback of translateService.translate() 
  // And it is simillar to the else statement in sendMessage() (yet, they are a bit different) 
  handleTranslatedOutPut(res, isSend, optionalMessageObject) {
    if (isSend)//we need to SEND this translated message...
    {
      this.inputMessage = res[0][0][0];
      this.inputMessage = encodeURIComponent(this.inputMessage);
      let messageToSent = {
        receiverId:this.chosenUser.agentId,
        senderId:this.globalVaribles.ourAgentID.value, content:this.inputMessage,
        contentLanguage:"English", isSenderAgent:true,
        sessionId:this.currChatID, DateTime:new Date(),
        isAgentToAgentMessage:true
      }
      this.signalRService.sendMessageToAgents(messageToSent);//used to send message to user
      this.message.send_message(this.currChatID, this.inputMessage, this.globalVaribles.ourAgentID.value,
                                this.chosenUser.agentId, true, "english", true).subscribe(nOp => void(0));//used to add message to DB
      this.inputMessage = decodeURIComponent(this.inputMessage);
      messageToSent.content = decodeURIComponent(this.decodingSpecialChars(messageToSent.content));
      this.allMessages.push(messageToSent);
      this.inputMessage="";//reset buffer
      this.scrollDown();
    }
    else//we need to receive that translated message...
    {
      optionalMessageObject.content = res[0][0][0];
      this.allMessages.push(optionalMessageObject);
    }

  }

  //Will indicate whenever another chat was chosen, and will call "conversationStart()"
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = JSON.stringify(chng.currentValue);
      this.enterConversationService.conversationStartAgents(this.chosenUser, this.globalVaribles.ourAgentID).subscribe(res => this.getResForChatStart(res));
    }
  }

  //will be callled on click
  translateMessages() {
    if (this.translateUserMessages) {
      for (let i = 0; i < this.allMessages.length; i++) {
        let currTranslatedMessage:string;
        this.translateService.translate(this.allMessages[i].content).subscribe(res => currTranslatedMessage = res[0][0][0]);
        this.allMessages[i]["shouldTranslate"] = currTranslatedMessage;//saving the translation
      }
    }
  }

  // This functions sends a message from agent to a agent:
  // (1) Calls signalr send message (it will send message on real time to user).
  // (2) Will add the message to DB using message.send_message
  // (3) Will add the message to the "allMessages" array that contains all messages in chat.
  // (4) Will scroll so the new message will apear when send
  sendMessage() {
    if (!this.inputMessage) {
      return;
    }
    this.inputMessage = this.encodingSpecialChars(this.inputMessage);
    if (this.translateMyMessages) {
      this.translateService.translate(this.inputMessage).subscribe(res => this.handleTranslatedOutPut(res, true, undefined));
    }
    else {
      this.inputMessage = encodeURIComponent(this.inputMessage);
      let messageToSent = {
        receiverId:this.chosenUser.agentId,
        senderId:this.globalVaribles.ourAgentID.value, content:this.inputMessage,
        contentLanguage:"English", isSenderAgent:true,
        sessionId:this.currChatID, DateTime:new Date(),
        isAgentToAgentMessage:true
      }
      this.signalRService.sendMessageToAgents(messageToSent);//used to send message to user
      this.message.send_message(this.currChatID, this.inputMessage, this.globalVaribles.ourAgentID, this.chosenUser.agentId, true, "english", true).subscribe(res => console.log(res));//used to add message to DB
      this.inputMessage = decodeURIComponent(this.inputMessage);
      messageToSent.content = decodeURIComponent(this.decodingSpecialChars(messageToSent.content));
      this.allMessages.push(messageToSent);
      this.inputMessage = "";//reset buffer
      this.scrollDown();
    }

  }
  private scrollDown() {
    setTimeout(function () {//do scrolling to the end of the chat
      var objDiv = document.getElementById("messagesContainer");
      objDiv.scrollTop = 100000;
    }, 0);
  }
  private encodingSpecialChars(orginalString) {
    orginalString = this.replaceAll('.', "***dotEncoding***", orginalString);
    orginalString = this.replaceAll('+', "***plusEncoding***", orginalString);
    orginalString = this.replaceAll("<", "***leftTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll(">", "***rightTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("?", "***questionMarkEncoding***", orginalString);
    orginalString = this.replaceAll("\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("\r\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("#", "***hashTagEncoding***", orginalString);
    orginalString = this.replaceAll("\\", "***backSlashEncoding***", orginalString);
    return orginalString;
  }
  private decodingSpecialChars(orginalString) {
    orginalString = this.replaceAll("***dotEncoding***", '.', orginalString);
    orginalString = this.replaceAll("***plusEncoding***", '+', orginalString);
    orginalString = this.replaceAll("***leftTringleBracketEncoding***", "<", orginalString);
    orginalString = this.replaceAll("***rightTringleBracketEncoding***", ">", orginalString);
    orginalString = this.replaceAll("***questionMarkEncoding***", "?", orginalString);
    orginalString = this.replaceAll("***newLineEncoding***", "<br/>", orginalString);
    orginalString = this.replaceAll("***newLineEncoding***", "<br/>", orginalString);
    orginalString = this.replaceAll("***hashTagEncoding***", "#", orginalString);
    orginalString = this.replaceAll("***backSlashEncoding***", "\\", orginalString);

    return orginalString;
  }

  //Will be called in the "startConversation()" callback.
  // (1) It will receive message history of the user we are talking to. 
  // (2) It will update allMessages to contain the history.
  getResForChatStart(res) {
    if (res["messageHistory"] != "" && res["messageHistory"]) {
      this.allMessages = res["messageHistory"];
    }
    this.currChatID = res["chatID"];
    this.globalVaribles.setUserID(this.currChatID);
  }

  // Define varibles for emoji keyboard:
  emojiKeyboard = false;
  emojiCatagory;

  emojis =
    [

      ["😀", "😁", "😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊", "😋", "😎", "😍", "😘", "😗", "😙", "😚", "☺️", "🙂", "🤗", "🤩", "🤔", "🤨", "😐", "😑", "😶", "🙄", "😏", "😣", "😥", "😮", "🤐", "😯", "😪", "😫", "😴", "😌", "😛", "😜", "😝", "🤤", "😒", "😓", "😔", "😕", "🙃", "🤑", "😲", "☹️", "🙁", "😖", "😞", "😟", "😤", "😢", "😭", "😦", "😧", "😨", "😩", "🤯", "😬", "😰", "😱", "😳", "🤪", "😵", "😡", "😠", "🤬", "😷", "🤒", "🤕", "🤢", "🤮", "🤧", "😇", "🤠", "🤡", "🤥", "🤫", "🤭", "🧐", "🤓", "😈", "👿", "👹", "👺", "💀", "👻", "👽", "🤖", "💩", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾"],
      ["👶", "👦", "👧", "👨", "👩", "👴", "👵", "👨‍⚕️", "👩‍⚕️", "👨‍🎓", "👩‍🎓", "👨‍⚖️", "👩‍⚖️", "👨‍🌾", "👩‍🌾", "👨‍🍳", "👩‍🍳", "👨‍🔧", "👩‍🔧", "👨‍🏭", "👩‍🏭", "👨‍💼", "👩‍💼", "👨‍🔬", "👩‍🔬", "👨‍💻", "👩‍💻", "👨‍🎤", "👩‍🎤", "👨‍🎨", "👩‍🎨", "👨‍✈️", "👩‍✈️", "👨‍🚀", "👩‍🚀", "👨‍🚒", "👩‍🚒", "👮", "👮‍♂️", "👮‍♀️", "🕵", "🕵️‍♂️", "🕵️‍♀️", "💂", "💂‍♂️", "💂‍♀️", "👷", "👷‍♂️", "👷‍♀️", "🤴", "👸", "👳", "👳‍♂️", "👳‍♀️", "👲", "🧕", "🧔", "👱", "👱‍♂️", "👱‍♀️", "🤵", "👰", "🤰", "🤱", "👼", "🎅", "🤶", "🧙‍♀️", "🧙‍♂️", "🧚‍♀️", "🧚‍♂️", "🧛‍♀️", "🧛‍♂️", "🧜‍♀️", "🧜‍♂️", "🧝‍♀️", "🧝‍♂️", "🧞‍♀️", "🧞‍♂️", "🧟‍♀️", "🧟‍♂️", "🙍", "🙍‍♂️", "🙍‍♀️", "🙎", "🙎‍♂️", "🙎‍♀️", "🙅", "🙅‍♂️", "🙅‍♀️", "🙆", "🙆‍♂️", "🙆‍♀️", "💁", "💁‍♂️", "💁‍♀️", "🙋", "🙋‍♂️", "🙋‍♀️", "🙇", "🙇‍♂️", "🙇‍♀️", "🤦", "🤦‍♂️", "🤦‍♀️", "🤷", "🤷‍♂️", "🤷‍♀️", "💆", "💆‍♂️", "💆‍♀️", "💇", "💇‍♂️", "💇‍♀️", "🚶", "🚶‍♂️", "🚶‍♀️", "🏃", "🏃‍♂️", "🏃‍♀️", "💃", "🕺", "👯", "👯‍♂️", "👯‍♀️", "🧖‍♀️", "🧖‍♂️", "🕴", "🗣", "👤", "👥", "👫", "👬", "👭", "💏", "👨‍❤️‍💋‍👨", "👩‍❤️‍💋‍👩", "💑", "👨‍❤️‍👨", "👩‍❤️‍👩", "👪", "👨‍👩‍👦", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧", "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👦", "👨‍👦‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👧‍👧", "👩‍👦", "👩‍👦‍👦", "👩‍👧", "👩‍👧‍👦", "👩‍👧‍👧", "🤳", "💪", "👈", "👉", "☝️", "👆", "🖕", "👇", "✌️", "🤞", "🖖", "🤘", "🖐", "✋", "👌", "👍", "👎", "✊", "👊", "🤛", "🤜", "🤚", "👋", "🤟", "✍️", "👏", "👐", "🙌", "🤲", "🙏", "🤝", "💅", "👂", "👃", "👣", "👀", "👁", "🧠", "👅", "👄", "💋"],
      ["👓", "🕶", "👔", "👕", "👖", "🧣", "🧤", "🧥", "🧦", "👗", "👘", "👙", "👚", "👛", "👜", "👝", "🎒", "👞", "👟", "👠", "👡", "👢", "👑", "👒", "🎩", "🎓", "🧢", "⛑", "💄", "💍", "🌂", "💼"],
      ["👐🏻", "🙌🏻", "👏🏻", "🙏🏻", "👍🏻", "👎🏻", "👊🏻", "✊🏻", "🤛🏻", "🤜🏻", "🤞🏻", "✌🏻", "🤘🏻", "👌🏻", "👈🏻", "👉🏻", "👆🏻", "👇🏻", "☝🏻", "✋🏻", "🤚🏻", "🖐🏻", "🖖🏻", "👋🏻", "🤙🏻", "💪🏻", "🖕🏻", "✍🏻", "🤳🏻", "💅🏻", "👂🏻", "👃🏻", "👶🏻", "👦🏻", "👧🏻", "👨🏻", "👩🏻", "👱🏻‍♀️", "👱🏻", "👴🏻", "👵🏻", "👲🏻", "👳🏻‍♀️", "👳🏻", "👮🏻‍♀️", "👮🏻", "👷🏻‍♀️", "👷🏻", "💂🏻‍♀️", "💂🏻", "🕵🏻‍♀️", "🕵🏻", "👩🏻‍⚕️", "👨🏻‍⚕️", "👩🏻‍🌾", "👨🏻‍🌾", "👩🏻‍🍳", "👨🏻‍🍳", "👩🏻‍🎓", "👨🏻‍🎓", "👩🏻‍🎤", "👨🏻‍🎤", "👩🏻‍🏫", "👨🏻‍🏫", "👩🏻‍🏭", "👨🏻‍🏭", "👩🏻‍💻", "👨🏻‍💻", "👩🏻‍💼", "👨🏻‍💼", "👩🏻‍🔧", "👨🏻‍🔧", "👩🏻‍🔬", "👨🏻‍🔬", "👩🏻‍🎨", "👨🏻‍🎨", "👩🏻‍🚒", "👨🏻‍🚒", "👩🏻‍✈️", "👨🏻‍✈️", "👩🏻‍🚀", "👨🏻‍🚀", "👩🏻‍⚖️", "👨🏻‍⚖️", "🤶🏻", "🎅🏻", "👸🏻", "🤴🏻", "👰🏻", "🤵🏻", "👼🏻", "🤰🏻", "🙇🏻‍♀️", "🙇🏻", "💁🏻", "💁🏻‍♂️", "🙅🏻", "🙅🏻‍♂️", "🙆🏻", "🙆🏻‍♂️", "🙋🏻", "🙋🏻‍♂️", "🤦🏻‍♀️", "🤦🏻‍♂️", "🤷🏻‍♀️", "🤷🏻‍♂️", "🙎🏻", "🙎🏻‍♂️", "🙍🏻", "🙍🏻‍♂️", "💇🏻", "💇🏻‍♂️", "💆🏻", "💆🏻‍♂️", "🕴🏻", "💃🏻", "🕺🏻", "🚶🏻‍♀️", "🚶🏻", "🏃🏻‍♀️", "🏃🏻", "🏋🏻‍♀️", "🏋🏻", "🤸🏻‍♀️", "🤸🏻‍♂️", "⛹🏻‍♀️", "⛹🏻", "🤾🏻‍♀️", "🤾🏻‍♂️", "🏌🏻‍♀️", "🏌🏻", "🏄🏻‍♀️", "🏄🏻", "🏊🏻‍♀️", "🏊🏻", "🤽🏻‍♀️", "🤽🏻‍♂️", "🚣🏻‍♀️", "🚣🏻", "🏇🏻", "🚴🏻‍♀️", "🚴🏻", "🚵🏻‍♀️", "🚵🏻", "🤹🏻‍♀️", "🤹🏻‍♂️", "🛀🏻"],
      ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼", "🐨", "🐯", "🦁", "🐮", "🐷", "🐽", "🐸", "🐵", "🙊", "🙉", "🙊", "🐒", "🐔", "🐧", "🐦", "🐤", "🐣", "🐥", "🦆", "🦅", "🦉", "🦇", "🐺", "🐗", "🐴", "🦄", "🐝", "🐛", "🦋", "🐌", "🐚", "🐞", "🐜", "🕷", "🕸", "🐢", "🐍", "🦎", "🦂", "🦀", "🦑", "🐙", "🦐", "🐠", "🐟", "🐡", "🐬", "🦈", "🐳", "🐋", "🐊", "🐆", "🐅", "🐃", "🐂", "🐄", "🦌", "🐪", "🐫", "🐘", "🦏", "🦍", "🐎", "🐖", "🐐", "🐏", "🐑", "🐕", "🐩", "🐈", "🐓", "🦃", "🕊", "🐇", "🐁", "🐀", "🐿", "🐾", "🐉", "🐲", "🌵", "🎄", "🌲", "🌳", "🌴", "🌱", "🌿", "☘️", "🍀", "🎍", "🎋", "🍃", "🍂", "🍁", "🍄", "🌾", "💐", "🌷", "🌹", "🥀", "🌻", "🌼", "🌸", "🌺", "🌎", "🌍", "🌏", "🌕", "🌖", "🌗", "🌘", "🌑", "🌒", "🌓", "🌔", "🌚", "🌝", "🌞", "🌛", "🌜", "🌙", "💫", "⭐️", "🌟", "✨", "⚡️", "🔥", "💥", "☄️", "☀️", "🌤", "⛅️", "🌥", "🌦", "🌈", "☁️", "🌧", "⛈", "🌩", "🌨", "☃️", "⛄️", "❄️", "🌬", "💨", "🌪", "🌫", "🌊", "💧", "💦", "☔️"],
      ["🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🍈", "🍒", "🍑", "🍍", "🥝", "🥑", "🍅", "🍆", "🥒", "🥕", "🌽", "🌶", "🥔", "🍠", "🌰", "🥜", "🍯", "🥐", "🍞", "🥖", "🧀", "🥚", "🍳", "🥓", "🥞", "🍤", "🍗", "🍖", "🍕", "🌭", "🍔", "🍟", "🥙", "🌮", "🌯", "🥗", "🥘", "🍝", "🍜", "🍲", "🍥", "🍣", "🍱", "🍛", "🍚", "🍙", "🍘", "🍢", "🍡", "🍧", "🍨", "🍦", "🍰", "🎂", "🍮", "🍭", "🍬", "🍫", "🍿", "🍩", "🍪", "🥛", "🍼", "☕️", "🍵", "🍶", "🍺", "🍻", "🥂", "🍷", "🥃", "🍸", "🍹", "🍾", "🥄", "🍴", "🍽"],
      ["⚽️", "🏀", "🏈", "⚾️", "🎾", "🏐", "🏉", "🎱", "🏓", "🏸", "🥅", "🏒", "🏑", "🏏", "⛳️", "🏹", "🎣", "🥊", "🥋", "⛸", "🎿", "⛷", "🏂", "🏋️‍♀️", "🏋️", "🤺", "🤼‍♀️", "🤼‍♂️", "🤸‍♀️", "🤸‍♂️", "⛹️‍♀️", "⛹️", "🤾‍♀️", "🤾‍♂️", "🏌️‍♀️", "🏌️", "🏄‍♀️", "🏄", "🏊‍♀️", "🏊", "🤽‍♀️", "🤽‍♂️", "🚣‍♀️", "🚣", "🏇", "🚴‍♀️", "🚴", "🚵‍♀️", "🚵", "🎽", "🏅", "🎖", "🥇", "🥈", "🥉", "🏆", "🏵", "🎗", "🎫", "🎟", "🎪", "🤹‍♀️", "🤹‍♂️", "🎭", "🎨", "🎬", "🎤", "🎧", "🎼", "🎹", "🥁", "🎷", "🎺", "🎸", "🎻", "🎲", "🎯", "🎳", "🎮", "🎰"],
      ["🚗", "🚕", "🚙", "🚌", "🚎", "🏎", "🚓", "🚑", "🚒", "🚐", "🚚", "🚛", "🚜", "🛴", "🚲", "🛵", "🏍", "🚨", "🚔", "🚍", "🚘", "🚖", "🚡", "🚠", "🚟", "🚃", "🚋", "🚞", "🚝", "🚄", "🚅", "🚈", "🚂", "🚆", "🚇", "🚊", "🚉", "🚁", "🛩", "✈️", "🛫", "🛬", "🚀", "🛰", "💺", "🛶", "⛵️", "🛥", "🚤", "🛳", "⛴", "🚢", "⚓️", "🚧", "⛽️", "🚏", "🚦", "🚥", "🗺", "🗿", "🗽", "⛲️", "🗼", "🏰", "🏯", "🏟", "🎡", "🎢", "🎠", "⛱", "🏖", "🏝", "⛰", "🏔", "🗻", "🌋", "🏜", "🏕", "⛺️", "🛤", "🛣", "🏗", "🏭", "🏠", "🏡", "🏘", "🏚", "🏢", "🏬", "🏣", "🏤", "🏥", "🏦", "🏨", "🏪", "🏫", "🏩", "💒", "🏛", "⛪️", "🕌", "🕍", "🕋", "⛩", "🗾", "🎑", "🏞", "🌅", "🌄", "🌠", "🎇", "🎆", "🌇", "🌆", "🏙", "🌃", "🌌", "🌉", "🌁"],
      ["⌚️", "📱", "📲", "💻", "⌨️", "🖥", "🖨", "🖱", "🖲", "🕹", "🗜", "💽", "💾", "💿", "📀", "📼", "📷", "📸", "📹", "🎥", "📽", "🎞", "📞", "☎️", "📟", "📠", "📺", "📻", "🎙", "🎚", "🎛", "⏱", "⏲", "⏰", "🕰", "⌛️", "⏳", "📡", "🔋", "🔌", "💡", "🔦", "🕯", "🗑", "🛢", "💸", "💵", "💴", "💶", "💷", "💰", "💳", "💎", "⚖️", "🔧", "🔨", "⚒", "🛠", "⛏", "🔩", "⚙️", "⛓", "🔫", "💣", "🔪", "🗡", "⚔️", "🛡", "🚬", "⚰️", "⚱️", "🏺", "🔮", "📿", "💈", "⚗️", "🔭", "🔬", "🕳", "💊", "💉", "🌡", "🚽", "🚰", "🚿", "🛁", "🛀", "🛎", "🔑", "🗝", "🚪", "🛋", "🛏", "🛌", "🖼", "🛍", "🛒", "🎁", "🎈", "🎏", "🎀", "🎊", "🎉", "🎎", "🏮", "🎐", "✉️", "📩", "📨", "📧", "💌", "📥", "📤", "📦", "🏷", "📪", "📫", "📬", "📭", "📮", "📯", "📜", "📃", "📄", "📑", "📊", "📈", "📉", "🗒", "🗓", "📆", "📅", "📇", "🗃", "🗳", "🗄", "📋", "📁", "📂", "🗂", "🗞", "📰", "📓", "📔", "📒", "📕", "📗", "📘", "📙", "📚", "📖", "🔖", "🔗", "📎", "🖇", "📐", "📏", "📌", "📍", "📌", "🎌", "🏳️", "🏴", "🏁", "🏳️‍🌈", "✂️", "🖊", "🖋", "✒️", "🖌", "🖍", "📝", "✏️", "🔍", "🔎", "🔏", "🔐", "🔒", "🔓"],
      ["❤️", "💛", "💚", "💙", "💜", "🖤", "💔", "❣️", "💕", "💞", "💓", "💗", "💖", "💘", "💝", "💟", "☮️", "✝️", "☪️", "🕉", "☸️", "✡️", "🔯", "🕎", "☯️", "☦️", "🛐", "⛎", "♈️", "♉️", "♊️", "♋️", "♌️", "♍️", "♎️", "♏️", "♐️", "♑️", "♒️", "♓️", "🆔", "⚛️", "🉑", "☢️", "☣️", "📴", "📳", "🈶", "🈚️", "🈸", "🈺", "🈷️", "✴️", "🆚", "💮", "🉐", "㊙️", "㊗️", "🈴", "🈵", "🈹", "🈲", "🅰️", "🅱️", "🆎", "🆑", "🅾️", "🆘", "❌", "⭕️", "🛑", "⛔️", "📛", "🚫", "💯", "💢", "♨️", "🚷", "🚯", "🚳", "🚱", "🔞", "📵", "🚭", "❗️", "❕", "❓", "❔", "‼️", "⁉️", "🔅", "🔆", "〽️", "⚠️", "🚸", "🔱", "⚜️", "🔰", "♻️", "✅", "🈯️", "💹", "❇️", "✳️", "❎", "🌐", "💠", "Ⓜ️", "🌀", "💤", "🏧", "🚾", "♿️", "🅿️", "🈳", "🈂️", "🛂", "🛃", "🛄", "🛅", "🚹", "🚺", "🚼", "🚻", "🚮", "🎦", "📶", "🈁", "🔣", "ℹ️", "🔤", "🔡", "🔠", "🆖", "🆗", "🆙", "🆒", "🆕", "🆓", "0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟", "🔢", "#️⃣", "*️⃣", "▶️", "⏸", "⏯", "⏹", "⏺", "⏭", "⏮", "⏩", "⏪", "⏫", "⏬", "◀️", "🔼", "🔽", "➡️", "⬅️", "⬆️", "⬇️", "↗️", "↘️", "↙️", "↖️", "↕️", "↔️", "↪️", "↩️", "⤴️", "⤵️", "🔀", "🔁", "🔂", "🔄", "🔃", "🎵", "🎶", "➕", "➖", "➗", "✖️", "💲", "💱", "™️", "©️", "®️", "〰️", "➰", "➿", "🔚", "🔙", "🔛", "🔝", "✔️", "☑️", "🔘", "⚪️", "⚫️", "🔴", "🔵", "🔺", "🔻", "🔸", "🔹", "🔶", "🔷", "🔳", "🔲", "▪️", "▫️", "◾️", "◽️", "◼️", "◻️", "⬛️", "⬜️", "🔈", "🔇", "🔉", "🔊", "🔔", "🔕", "📣", "📢", "👁‍🗨", "💬", "💭", "🗯", "♠️", "♣️", "♥️", "♦️", "🃏", "🎴", "🀄️", "🕐", "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚", "🕛", "🕜", "🕝", "🕞", "🕟", "🕠", "🕡", "🕢", "🕣", "🕤", "🕥", "🕦", "🕧"]

    ];

  // Called when an emoji is chosen from the emoji keyboard.
  // (1) Will add an emoji to curr message
  addEmojiToCurrInput(indexOfEmojiRow, indexOfEmojiCol) {
    this.inputMessage += this.emojis[indexOfEmojiRow][indexOfEmojiCol];
  }

  // Called when a canned message is chosen from the canned message bar.
  // (1) Will add the canned message content to curr message
  addMessageToCurrInput(indexOfMessage) {
    this.inputMessage += this.cannedMessages[indexOfMessage].content;
  }
}