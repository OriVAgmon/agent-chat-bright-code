import { Injectable } from '@angular/core';
import { Response } from '@angular/http';  
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';
  
import { environment } from '../../../../environments/environment';

@Injectable()
export class MessageService {

  constructor(private http: HttpClient) { 
    
  }
  baseUrl:string = environment.baseUrl;
  public send_message(sessionID: number, content: string,
                     senderID: number, receiverID:number,
                      isSenderAgent: boolean, contentLanguage: string,
                       isAgentToAgentMessage: boolean):Observable<number>{
    content = this.encodingSpecialChars(content);
    var uri = "http://" + this.baseUrl + "/api/messages/add/"+sessionID+'/'+senderID+'/'+receiverID+'/'+content+'/'+contentLanguage+'/'+isSenderAgent+'/'+isAgentToAgentMessage;
    return this.http.get<number>(uri);
  }
  private encodingSpecialChars(orginalString):string{
    orginalString = this.replaceAll(".", "***dotEncoding***", orginalString);
    orginalString = this.replaceAll("?", "***questionMarkEncoding***", orginalString);    
    orginalString = this.replaceAll("%3C", "***leftTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("%3E", "***rightTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("%2B", "***plusEncoding***", orginalString);    
    orginalString = this.replaceAll("%2F", "***slashEncoding***", orginalString);
    orginalString = this.replaceAll("%5C", "***backSlashEncoding***", orginalString);
    orginalString = this.replaceAll("%0A", "***newLineEncoding***", orginalString);
    return orginalString;
  }
  private replaceAll(src:string, dst:string, stringToOperateOn:string):string{

    while(stringToOperateOn.indexOf(src) != -1)
    {
      stringToOperateOn = stringToOperateOn.replace(src, dst);
    }    
    return stringToOperateOn;
  }


}
