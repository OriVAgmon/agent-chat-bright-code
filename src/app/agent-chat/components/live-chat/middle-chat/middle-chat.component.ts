import { Component, Input, OnChanges, SimpleChanges, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from '../../../shared/services/message.service';
import { AllCannedMessagesService } from '../../../shared/services/all-canned-messages.service';
import { GetChatsHistoryService } from '../../../shared/services/get-chats-history.service';
import { EnterConversationService } from '../../../shared/services/enter-conversation.service';
import { GlobalVariblesService } from './../../../shared/services/global-varibles.service'
import { SingalRService } from '../../../shared/services/singal-r.service';
import { TranslateService } from './../../../shared/services/translate.service';
import { CheckPermissionToSendMessagesToAgentService } from './../../../shared/services/check-permission-to-send-messages-to-agent.service';

import { User } from '../../../shared/models/user-model';
import { Message } from '../../../shared/models/message-model';
import { CannedMessage } from '../../../shared/models/canned-message-model';

@Component({
  selector: 'app-middle-chat',
  templateUrl: './middle-chat.component.html',
  styleUrls: ['./middle-chat.component.scss'],
  providers: [
              AllCannedMessagesService,
              GetChatsHistoryService,
              EnterConversationService,
              TranslateService,
              CheckPermissionToSendMessagesToAgentService
              ]
})
export class MiddleChatComponent implements OnInit{
  
  //Varibles:
  @Input() chosenUser: User;
  agentImg:string = "assets/profile_teaser.png";
  userImg:string = "assets/user.png";
  allMessages:Message[] = [];
  cannedMessages:CannedMessage[] = [];
  translateMyMessages:boolean;
  translateUserMessages:boolean;
  hasMessages=false;
  inputMessage:string;
  currAgentID:number = 1;
  currChatID:number;
  shouldScrollDown = false;
  quickReply = false;
  fileUpload=false;
  hasPermissionToSendMessages=false;
  currMessageContent: string;
  sendMessagesPermission=true;
  sentNoticeTranslationMessage=false;

  constructor(private message: MessageService, private allCannedMessages: AllCannedMessagesService, 
              private specificChatHistory: GetChatsHistoryService, private enterConversationService: EnterConversationService,
              private signalRService: SingalRService, private globalVaribles: GlobalVariblesService, private translateService: TranslateService,
              private checkSendMessagesPermission:CheckPermissionToSendMessagesToAgentService){   

                this.allCannedMessages.getAllCannedMessagesOfAgentById(this.currAgentID).subscribe(cannedMessages=>this.cannedMessages=cannedMessages);
                this.signalRService.startConn(false);//will start the connection if it wasnt started already.
              }  
  
  ngOnInit(){
    this.signalRService.observableLatestMessage.subscribe(m=>this.recMessage(m));//will get the latest message from signalr service in real time.
    
    //transfer user to another agent
    this.globalVaribles.gotTransferedUser.subscribe(uid=> {if(uid == this.chosenUser.userID)  this.sendMessagesPermission = true} );
    
    //we transfered a user to another agent so we have no messages sending premmistion
    this.globalVaribles.weJustTransferedAUser.subscribe(uid=> {if(uid == this.chosenUser.userID)  this.sendMessagesPermission = false} );

    //scroll down
    this.scrollDown();    
  }
  //will be callled on click
  translateMessages():void{
    if (this.translateUserMessages)
    {
      for(let i = 0; i < this.allMessages.length; i++)
      {
        if(!this.allMessages[i].isSenderAgent)//translate only user messages
        {
          let currTranslatedMessage:string;
          this.translateService.translate(this.allMessages[i].content).subscribe(res=> this.allMessages[i]["shouldTranslate"] = res[0][0][0]);      
        }
      }
    }
  }
  // This function receives a message:
  // (1) Will decode the message.
  // (2) Will translate it (if needed).
  // (3) Will push the new message to the message array ("allMessages").
  recMessage(m:Message):void{
    console.log(m != null && m.senderId == this.chosenUser.userID);
    if(m != null && m.senderId == this.chosenUser.userID)
    {   
      m.content = decodeURIComponent(m.content);
      m.content = this.decodingSpecialChars(m.content);
      if(this.translateUserMessages)
      {        
        this.translateService.translate(m.content).subscribe(res=> this.handleTranslatedOutPut(res, false, m, undefined));      
      }
      else
      {
        this.allMessages.push(m);
      }
     this.hasMessages = true;
     this.scrollDown();
    }
  }


  //Will indicate whenever another chat was chosen, and will call "conversationStart()"
  ngOnChanges(changes: SimpleChanges){
    for(let propName in changes)
    {
      let chng = changes[propName];
      let cur  = JSON.stringify(chng.currentValue); 
      this.enterConversationService.conversationStart(this.chosenUser, this.globalVaribles.ourAgentID)
                                    .subscribe(res=> this.handleResForChatStart(res));      
    }
  }



  // This functions sends a message from agent to a user:
  // (1) Calls signalr send message (it will send message on real time to user).
  // (2) Will add the message to DB using message.send_message
  // (3) Will add the message to the "allMessages" array that contains all messages in chat.
  // (4) Will scroll so the new message will apear when send
  sendMessage():void{
    if (!this.inputMessage || !this.sendMessagesPermission)//if this.sendMessagesPermission is false we cant send messages
    {
      return;
    }
    this.inputMessage = this.encodingSpecialChars(this.inputMessage);
    
    if(this.translateMyMessages)
    {
      if(!this.sentNoticeTranslationMessage)
      {
        this.sentNoticeTranslationMessage = true;  
        this.sendNoticeTranslationServiceMessage();
      }
      this.translateService.translate(this.inputMessage).subscribe(res=> this.handleTranslatedOutPut(res, true, undefined, this.inputMessage));      
    }
    else
    {
      this.inputMessage = encodeURIComponent(this.inputMessage);    
      let messageToSent = {
                            receiverId:this.chosenUser.userID,
                            senderId:this.currAgentID, content:this.inputMessage,
                            contentLanguage:"English", isSenderAgent:true,
                            sessionId:this.currChatID, DateTime:new Date()
                          }
                          
      this.signalRService.sendMessage(messageToSent);//used to send message to user in !real time!
      this.message.send_message(this.currChatID, this.inputMessage, this.currAgentID, this.chosenUser.userID, true, "english", false).subscribe(res=>console.log(res));//used to add message to DB
      this.inputMessage = decodeURIComponent(this.inputMessage);
      messageToSent.content = decodeURIComponent(this.decodingSpecialChars(messageToSent.content));
      this.allMessages.push(messageToSent);
      this.inputMessage = "";//reset buffer
      this.scrollDown();
    }

  }


  // This function is the callback of translateService.translate(),
  // And it is simillar (yet, different) to the else statement in sendMessage()
  handleTranslatedOutPut(res, isSend, optionalMessageObject, optionalOriginalMessage):void{
    if(isSend)//we need to SEND this translated message...
    {
      this.inputMessage = res[0][0][0];
      this.inputMessage = encodeURIComponent(this.inputMessage);    
      let messageToSent = {
        receiverId:this.chosenUser.userID,
        senderId:this.currAgentID, content:this.inputMessage,
        contentLanguage:"English", isSenderAgent:true,
        sessionId:this.currChatID, DateTime:new Date(),
        isAgentToAgentMessage:false
      };
      this.signalRService.sendMessage(messageToSent);//used to send message to user
      this.message.send_message(this.currChatID, this.inputMessage, this.currAgentID, this.chosenUser.userID, true, "english", false).subscribe(nOp=>console.log(nOp));//used to add message to DB
      this.inputMessage = decodeURIComponent(this.inputMessage);
      messageToSent.content = decodeURIComponent(this.decodingSpecialChars(messageToSent.content));
      messageToSent["shouldTranslate"]=res[0][0][0];
      messageToSent.content=optionalOriginalMessage;
      this.allMessages.push(messageToSent);
      this.inputMessage = "";//reset buffer
      this.scrollDown();
    }
    else//we need to receive that translated message...
    {
      if(optionalMessageObject.content != res[0][0][0])
      {
        optionalMessageObject["shouldTranslate"] = res[0][0][0];
      }
      this.allMessages.push(optionalMessageObject);
    }
  }

  //Will be called in the "startConversation()" callback.
  // (1) It will receive message history of the user we are talking to. 
  // (2) It will update allMessages to contain the history.
  handleResForChatStart(res):void{
    this.allMessages = [];
    if(res["messageHistory"] != "" && res["messageHistory"])
    {
      this.allMessages = res["messageHistory"];
    }
    this.currChatID = res["chatID"];
    this.globalVaribles.chatID.next(this.currChatID);  
    this.checkSendMessagesPermission.checkPermissionToSendMessages(this.globalVaribles.ourAgentID, this.currChatID)
    .subscribe(hasPermission=>this.sendMessagesPermission=hasPermission);

  }
  sendNoticeTranslationServiceMessage():void{
    let message = "Dear client, this module is using Google translate to operate this chat. We are sorry for any miss translation that may be caused through this chat session";
    message = encodeURIComponent(message);    
    let messageToSent = {
      receiverId:this.chosenUser.userID,
      senderId:this.currAgentID, content:message,
      contentLanguage:"English", isSenderAgent:true,
      sessionId:this.currChatID, DateTime:new Date(),
      isAgentToAgentMessage:false
    };
    this.signalRService.sendMessage(messageToSent);//used to send message to user
    this.message.send_message(this.currChatID, message, this.currAgentID, this.chosenUser.userID, true, "english", false).subscribe(res=>console.log(res));//used to add message to DB
    message = decodeURIComponent(message);
    messageToSent.content = decodeURIComponent(this.decodingSpecialChars(messageToSent.content));
    this.allMessages.push(messageToSent);
    this.scrollDown();
  }

  // Define varibles for emoji keyboard:
  emojiKeyboard = false;
  emojiCatagory;
  emojis=
  [
    ["😀","😁","😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊", "😋", "😎", "😍", "😘", "😗", "😙", "😚", "☺️", "🙂", "🤗", "🤩", "🤔", "🤨", "😐", "😑", "😶", "🙄", "😏", "😣", "😥", "😮", "🤐", "😯", "😪", "😫", "😴", "😌", "😛", "😜", "😝", "🤤", "😒", "😓", "😔", "😕", "🙃", "🤑", "😲", "☹️", "🙁", "😖", "😞", "😟", "😤", "😢", "😭", "😦", "😧", "😨", "😩", "🤯", "😬", "😰", "😱", "😳", "🤪", "😵", "😡", "😠", "🤬", "😷", "🤒", "🤕", "🤢", "🤮", "🤧", "😇", "🤠", "🤡", "🤥", "🤫", "🤭", "🧐", "🤓", "😈", "👿", "👹", "👺", "💀", "👻", "👽", "🤖", "💩", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾"],
    ["👶", "👦", "👧", "👨", "👩", "👴", "👵", "👨‍⚕️", "👩‍⚕️", "👨‍🎓", "👩‍🎓", "👨‍⚖️", "👩‍⚖️", "👨‍🌾", "👩‍🌾", "👨‍🍳", "👩‍🍳", "👨‍🔧", "👩‍🔧", "👨‍🏭", "👩‍🏭", "👨‍💼", "👩‍💼", "👨‍🔬", "👩‍🔬", "👨‍💻", "👩‍💻", "👨‍🎤", "👩‍🎤", "👨‍🎨", "👩‍🎨", "👨‍✈️", "👩‍✈️", "👨‍🚀", "👩‍🚀", "👨‍🚒", "👩‍🚒", "👮", "👮‍♂️", "👮‍♀️", "🕵", "🕵️‍♂️", "🕵️‍♀️", "💂", "💂‍♂️", "💂‍♀️", "👷", "👷‍♂️", "👷‍♀️", "🤴", "👸", "👳", "👳‍♂️", "👳‍♀️", "👲", "🧕", "🧔", "👱", "👱‍♂️", "👱‍♀️", "🤵", "👰", "🤰", "🤱", "👼", "🎅", "🤶", "🧙‍♀️", "🧙‍♂️", "🧚‍♀️", "🧚‍♂️", "🧛‍♀️", "🧛‍♂️", "🧜‍♀️", "🧜‍♂️", "🧝‍♀️", "🧝‍♂️", "🧞‍♀️", "🧞‍♂️", "🧟‍♀️", "🧟‍♂️", "🙍", "🙍‍♂️", "🙍‍♀️", "🙎", "🙎‍♂️", "🙎‍♀️", "🙅", "🙅‍♂️", "🙅‍♀️", "🙆", "🙆‍♂️", "🙆‍♀️", "💁", "💁‍♂️", "💁‍♀️", "🙋", "🙋‍♂️", "🙋‍♀️", "🙇", "🙇‍♂️", "🙇‍♀️", "🤦", "🤦‍♂️", "🤦‍♀️", "🤷", "🤷‍♂️", "🤷‍♀️", "💆", "💆‍♂️", "💆‍♀️", "💇", "💇‍♂️", "💇‍♀️", "🚶", "🚶‍♂️", "🚶‍♀️", "🏃", "🏃‍♂️", "🏃‍♀️", "💃", "🕺", "👯", "👯‍♂️", "👯‍♀️", "🧖‍♀️", "🧖‍♂️", "🕴", "🗣", "👤", "👥", "👫", "👬", "👭", "💏", "👨‍❤️‍💋‍👨", "👩‍❤️‍💋‍👩", "💑", "👨‍❤️‍👨", "👩‍❤️‍👩", "👪", "👨‍👩‍👦", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧", "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👦", "👨‍👦‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👧‍👧", "👩‍👦", "👩‍👦‍👦", "👩‍👧", "👩‍👧‍👦", "👩‍👧‍👧", "🤳", "💪", "👈", "👉", "☝️", "👆", "🖕", "👇", "✌️", "🤞", "🖖", "🤘", "🖐", "✋", "👌", "👍", "👎", "✊", "👊", "🤛", "🤜", "🤚", "👋", "🤟", "✍️", "👏", "👐", "🙌", "🤲", "🙏", "🤝", "💅", "👂", "👃", "👣", "👀", "👁", "🧠", "👅", "👄", "💋"  ],
    ["👓", "🕶", "👔", "👕", "👖", "🧣", "🧤", "🧥", "🧦", "👗", "👘", "👙", "👚", "👛", "👜", "👝", "🎒", "👞", "👟", "👠", "👡", "👢", "👑", "👒", "🎩", "🎓", "🧢", "⛑", "💄", "💍", "🌂", "💼"],
    ["👐🏻", "🙌🏻", "👏🏻", "🙏🏻", "👍🏻", "👎🏻", "👊🏻", "✊🏻", "🤛🏻", "🤜🏻", "🤞🏻", "✌🏻", "🤘🏻", "👌🏻", "👈🏻", "👉🏻", "👆🏻", "👇🏻", "☝🏻", "✋🏻", "🤚🏻", "🖐🏻", "🖖🏻", "👋🏻", "🤙🏻", "💪🏻", "🖕🏻", "✍🏻", "🤳🏻", "💅🏻", "👂🏻", "👃🏻", "👶🏻", "👦🏻", "👧🏻", "👨🏻", "👩🏻", "👱🏻‍♀️", "👱🏻", "👴🏻", "👵🏻", "👲🏻", "👳🏻‍♀️", "👳🏻", "👮🏻‍♀️", "👮🏻", "👷🏻‍♀️", "👷🏻", "💂🏻‍♀️", "💂🏻", "🕵🏻‍♀️", "🕵🏻", "👩🏻‍⚕️", "👨🏻‍⚕️", "👩🏻‍🌾", "👨🏻‍🌾", "👩🏻‍🍳", "👨🏻‍🍳", "👩🏻‍🎓", "👨🏻‍🎓", "👩🏻‍🎤", "👨🏻‍🎤", "👩🏻‍🏫", "👨🏻‍🏫", "👩🏻‍🏭", "👨🏻‍🏭", "👩🏻‍💻", "👨🏻‍💻", "👩🏻‍💼", "👨🏻‍💼", "👩🏻‍🔧", "👨🏻‍🔧", "👩🏻‍🔬", "👨🏻‍🔬", "👩🏻‍🎨", "👨🏻‍🎨", "👩🏻‍🚒", "👨🏻‍🚒", "👩🏻‍✈️", "👨🏻‍✈️", "👩🏻‍🚀", "👨🏻‍🚀", "👩🏻‍⚖️", "👨🏻‍⚖️", "🤶🏻", "🎅🏻", "👸🏻", "🤴🏻", "👰🏻", "🤵🏻", "👼🏻", "🤰🏻", "🙇🏻‍♀️", "🙇🏻", "💁🏻", "💁🏻‍♂️", "🙅🏻", "🙅🏻‍♂️", "🙆🏻", "🙆🏻‍♂️", "🙋🏻", "🙋🏻‍♂️", "🤦🏻‍♀️", "🤦🏻‍♂️", "🤷🏻‍♀️", "🤷🏻‍♂️", "🙎🏻", "🙎🏻‍♂️", "🙍🏻", "🙍🏻‍♂️", "💇🏻", "💇🏻‍♂️", "💆🏻", "💆🏻‍♂️", "🕴🏻", "💃🏻", "🕺🏻", "🚶🏻‍♀️", "🚶🏻", "🏃🏻‍♀️", "🏃🏻", "🏋🏻‍♀️", "🏋🏻", "🤸🏻‍♀️", "🤸🏻‍♂️", "⛹🏻‍♀️", "⛹🏻", "🤾🏻‍♀️", "🤾🏻‍♂️", "🏌🏻‍♀️", "🏌🏻", "🏄🏻‍♀️", "🏄🏻", "🏊🏻‍♀️", "🏊🏻", "🤽🏻‍♀️", "🤽🏻‍♂️", "🚣🏻‍♀️", "🚣🏻", "🏇🏻", "🚴🏻‍♀️", "🚴🏻", "🚵🏻‍♀️", "🚵🏻", "🤹🏻‍♀️", "🤹🏻‍♂️", "🛀🏻"],
    ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼", "🐨", "🐯", "🦁", "🐮", "🐷", "🐽", "🐸", "🐵", "🙊", "🙉", "🙊", "🐒", "🐔", "🐧", "🐦", "🐤", "🐣", "🐥", "🦆", "🦅", "🦉", "🦇", "🐺", "🐗", "🐴", "🦄", "🐝", "🐛", "🦋", "🐌", "🐚", "🐞", "🐜", "🕷", "🕸", "🐢", "🐍", "🦎", "🦂", "🦀", "🦑", "🐙", "🦐", "🐠", "🐟", "🐡", "🐬", "🦈", "🐳", "🐋", "🐊", "🐆", "🐅", "🐃", "🐂", "🐄", "🦌", "🐪", "🐫", "🐘", "🦏", "🦍", "🐎", "🐖", "🐐", "🐏", "🐑", "🐕", "🐩", "🐈", "🐓", "🦃", "🕊", "🐇", "🐁", "🐀", "🐿", "🐾", "🐉", "🐲", "🌵", "🎄", "🌲", "🌳", "🌴", "🌱", "🌿", "☘️", "🍀", "🎍", "🎋", "🍃", "🍂", "🍁", "🍄", "🌾", "💐", "🌷", "🌹", "🥀", "🌻", "🌼", "🌸", "🌺", "🌎", "🌍", "🌏", "🌕", "🌖", "🌗", "🌘", "🌑", "🌒", "🌓", "🌔", "🌚", "🌝", "🌞", "🌛", "🌜", "🌙", "💫", "⭐️", "🌟", "✨", "⚡️", "🔥", "💥", "☄️", "☀️", "🌤", "⛅️", "🌥", "🌦", "🌈", "☁️", "🌧", "⛈", "🌩", "🌨", "☃️", "⛄️", "❄️", "🌬", "💨", "🌪", "🌫", "🌊", "💧", "💦", "☔️"],
    ["🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🍈", "🍒", "🍑", "🍍", "🥝", "🥑", "🍅", "🍆", "🥒", "🥕", "🌽", "🌶", "🥔", "🍠", "🌰", "🥜", "🍯", "🥐", "🍞", "🥖", "🧀", "🥚", "🍳", "🥓", "🥞", "🍤", "🍗", "🍖", "🍕", "🌭", "🍔", "🍟", "🥙", "🌮", "🌯", "🥗", "🥘", "🍝", "🍜", "🍲", "🍥", "🍣", "🍱", "🍛", "🍚", "🍙", "🍘", "🍢", "🍡", "🍧", "🍨", "🍦", "🍰", "🎂", "🍮", "🍭", "🍬", "🍫", "🍿", "🍩", "🍪", "🥛", "🍼", "☕️", "🍵", "🍶", "🍺", "🍻", "🥂", "🍷", "🥃", "🍸", "🍹", "🍾", "🥄", "🍴", "🍽"],
    ["⚽️", "🏀", "🏈", "⚾️", "🎾", "🏐", "🏉", "🎱", "🏓", "🏸", "🥅", "🏒", "🏑", "🏏", "⛳️", "🏹", "🎣", "🥊", "🥋", "⛸", "🎿", "⛷", "🏂", "🏋️‍♀️", "🏋️", "🤺", "🤼‍♀️", "🤼‍♂️", "🤸‍♀️", "🤸‍♂️", "⛹️‍♀️", "⛹️", "🤾‍♀️", "🤾‍♂️", "🏌️‍♀️", "🏌️", "🏄‍♀️", "🏄", "🏊‍♀️", "🏊", "🤽‍♀️", "🤽‍♂️", "🚣‍♀️", "🚣", "🏇", "🚴‍♀️", "🚴", "🚵‍♀️", "🚵", "🎽", "🏅", "🎖", "🥇", "🥈", "🥉", "🏆", "🏵", "🎗", "🎫", "🎟", "🎪", "🤹‍♀️", "🤹‍♂️", "🎭", "🎨", "🎬", "🎤", "🎧", "🎼", "🎹", "🥁", "🎷", "🎺", "🎸", "🎻", "🎲", "🎯", "🎳", "🎮", "🎰"],
    ["🚗", "🚕", "🚙", "🚌", "🚎", "🏎", "🚓", "🚑", "🚒", "🚐", "🚚", "🚛", "🚜", "🛴", "🚲", "🛵", "🏍", "🚨", "🚔", "🚍", "🚘", "🚖", "🚡", "🚠", "🚟", "🚃", "🚋", "🚞", "🚝", "🚄", "🚅", "🚈", "🚂", "🚆", "🚇", "🚊", "🚉", "🚁", "🛩", "✈️", "🛫", "🛬", "🚀", "🛰", "💺", "🛶", "⛵️", "🛥", "🚤", "🛳", "⛴", "🚢", "⚓️", "🚧", "⛽️", "🚏", "🚦", "🚥", "🗺", "🗿", "🗽", "⛲️", "🗼", "🏰", "🏯", "🏟", "🎡", "🎢", "🎠", "⛱", "🏖", "🏝", "⛰", "🏔", "🗻", "🌋", "🏜", "🏕", "⛺️", "🛤", "🛣", "🏗", "🏭", "🏠", "🏡", "🏘", "🏚", "🏢", "🏬", "🏣", "🏤", "🏥", "🏦", "🏨", "🏪", "🏫", "🏩", "💒", "🏛", "⛪️", "🕌", "🕍", "🕋", "⛩", "🗾", "🎑", "🏞", "🌅", "🌄", "🌠", "🎇", "🎆", "🌇", "🌆", "🏙", "🌃", "🌌", "🌉", "🌁"],
    ["⌚️", "📱", "📲", "💻", "⌨️", "🖥", "🖨", "🖱", "🖲", "🕹", "🗜", "💽", "💾", "💿", "📀", "📼", "📷", "📸", "📹", "🎥", "📽", "🎞", "📞", "☎️", "📟", "📠", "📺", "📻", "🎙", "🎚", "🎛", "⏱", "⏲", "⏰", "🕰", "⌛️", "⏳", "📡", "🔋", "🔌", "💡", "🔦", "🕯", "🗑", "🛢", "💸", "💵", "💴", "💶", "💷", "💰", "💳", "💎", "⚖️", "🔧", "🔨", "⚒", "🛠", "⛏", "🔩", "⚙️", "⛓", "🔫", "💣", "🔪", "🗡", "⚔️", "🛡", "🚬", "⚰️", "⚱️", "🏺", "🔮", "📿", "💈", "⚗️", "🔭", "🔬", "🕳", "💊", "💉", "🌡", "🚽", "🚰", "🚿", "🛁", "🛀", "🛎", "🔑", "🗝", "🚪", "🛋", "🛏", "🛌", "🖼", "🛍", "🛒", "🎁", "🎈", "🎏", "🎀", "🎊", "🎉", "🎎", "🏮", "🎐", "✉️", "📩", "📨", "📧", "💌", "📥", "📤", "📦", "🏷", "📪", "📫", "📬", "📭", "📮", "📯", "📜", "📃", "📄", "📑", "📊", "📈", "📉", "🗒", "🗓", "📆", "📅", "📇", "🗃", "🗳", "🗄", "📋", "📁", "📂", "🗂", "🗞", "📰", "📓", "📔", "📒", "📕", "📗", "📘", "📙", "📚", "📖", "🔖", "🔗", "📎", "🖇", "📐", "📏", "📌", "📍", "📌", "🎌", "🏳️", "🏴", "🏁", "🏳️‍🌈", "✂️", "🖊", "🖋", "✒️", "🖌", "🖍", "📝", "✏️", "🔍", "🔎", "🔏", "🔐", "🔒", "🔓"],
    ["❤️", "💛", "💚", "💙", "💜", "🖤", "💔", "❣️", "💕", "💞", "💓", "💗", "💖", "💘", "💝", "💟", "☮️", "✝️", "☪️", "🕉", "☸️", "✡️", "🔯", "🕎", "☯️", "☦️", "🛐", "⛎", "♈️", "♉️", "♊️", "♋️", "♌️", "♍️", "♎️", "♏️", "♐️", "♑️", "♒️", "♓️", "🆔", "⚛️", "🉑", "☢️", "☣️", "📴", "📳", "🈶", "🈚️", "🈸", "🈺", "🈷️", "✴️", "🆚", "💮", "🉐", "㊙️", "㊗️", "🈴", "🈵", "🈹", "🈲", "🅰️", "🅱️", "🆎", "🆑", "🅾️", "🆘", "❌", "⭕️", "🛑", "⛔️", "📛", "🚫", "💯", "💢", "♨️", "🚷", "🚯", "🚳", "🚱", "🔞", "📵", "🚭", "❗️", "❕", "❓", "❔", "‼️", "⁉️", "🔅", "🔆", "〽️", "⚠️", "🚸", "🔱", "⚜️", "🔰", "♻️", "✅", "🈯️", "💹", "❇️", "✳️", "❎", "🌐", "💠", "Ⓜ️", "🌀", "💤", "🏧", "🚾", "♿️", "🅿️", "🈳", "🈂️", "🛂", "🛃", "🛄", "🛅", "🚹", "🚺", "🚼", "🚻", "🚮", "🎦", "📶", "🈁", "🔣", "ℹ️", "🔤", "🔡", "🔠", "🆖", "🆗", "🆙", "🆒", "🆕", "🆓", "0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟", "🔢", "#️⃣", "*️⃣", "▶️", "⏸", "⏯", "⏹", "⏺", "⏭", "⏮", "⏩", "⏪", "⏫", "⏬", "◀️", "🔼", "🔽", "➡️", "⬅️", "⬆️", "⬇️", "↗️", "↘️", "↙️", "↖️", "↕️", "↔️", "↪️", "↩️", "⤴️", "⤵️", "🔀", "🔁", "🔂", "🔄", "🔃", "🎵", "🎶", "➕", "➖", "➗", "✖️", "💲", "💱", "™️", "©️", "®️", "〰️", "➰", "➿", "🔚", "🔙", "🔛", "🔝", "✔️", "☑️", "🔘", "⚪️", "⚫️", "🔴", "🔵", "🔺", "🔻", "🔸", "🔹", "🔶", "🔷", "🔳", "🔲", "▪️", "▫️", "◾️", "◽️", "◼️", "◻️", "⬛️", "⬜️", "🔈", "🔇", "🔉", "🔊", "🔔", "🔕", "📣", "📢", "👁‍🗨", "💬", "💭", "🗯", "♠️", "♣️", "♥️", "♦️", "🃏", "🎴", "🀄️", "🕐", "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚", "🕛", "🕜", "🕝", "🕞", "🕟", "🕠", "🕡", "🕢", "🕣", "🕤", "🕥", "🕦", "🕧"]  
  ];

  // Called when an emoji is chosen from the emoji keyboard.
  // (1) Will add an emoji to curr message
  addEmojiToCurrInput(indexOfEmojiRow, indexOfEmojiCol):void{
    this.inputMessage += this.emojis[indexOfEmojiRow][indexOfEmojiCol];
  }

  // Called when a canned message is chosen from the canned message bar.
  // (1) Will add the canned message content to curr message
  addMessageToCurrInput(indexOfMessage):void{
    this.inputMessage += this.cannedMessages[indexOfMessage].content;
  }

  //decoding & enconding 3 functions:
  // (1)
  private replaceAll(src:string, dst:string, stringToOperateOn:string):string{
    while(stringToOperateOn.indexOf(src) != -1)
    {
      stringToOperateOn = stringToOperateOn.replace(src, dst);
    }    
    return stringToOperateOn;
  }
  // (2)
  private encodingSpecialChars(orginalString):string{
    orginalString = this.replaceAll('.', "***dotEncoding***", orginalString);
    orginalString = this.replaceAll('+', "***plusEncoding***", orginalString);
    orginalString = this.replaceAll("<", "***leftTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll(">", "***rightTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("?", "***questionMarkEncoding***", orginalString);
    orginalString = this.replaceAll("\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("\r\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("#", "***hashTagEncoding***", orginalString);
    return orginalString;
  }
  //(3)
  private decodingSpecialChars(orginalString):string{
    orginalString = this.replaceAll("***dotEncoding***", '.', orginalString);
    orginalString = this.replaceAll( "***plusEncoding***", '+', orginalString);
    orginalString = this.replaceAll("***leftTringleBracketEncoding***", "<", orginalString);
    orginalString = this.replaceAll("***rightTringleBracketEncoding***", ">", orginalString);
    orginalString = this.replaceAll("***questionMarkEncoding***","?", orginalString);
    orginalString = this.replaceAll("***newLineEncoding***","<br/>", orginalString);
    orginalString = this.replaceAll("***newLineEncoding***", "<br/>", orginalString);
    orginalString = this.replaceAll("***hashTagEncoding***", "#", orginalString);
    return orginalString;
  }

  //(4)
  private scrollDown():void{
    setTimeout(function(){//do scrolling to the end of the chat
      var objDiv = document.getElementById("messagesContainer");
      objDiv.scrollTop = 100000;
      },0);
  }

}