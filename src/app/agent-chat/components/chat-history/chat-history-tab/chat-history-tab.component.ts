import { Component, OnInit } from '@angular/core';
import { PassChosenChatIdService } from '../pass-chosen-chat-id.service';
import { GetChatsHistoryService } from '../../../shared/services/get-chats-history.service';
import { GlobalVariblesService } from '../../../shared/services/global-varibles.service';
import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'app-chat-history-tab',
  templateUrl: './chat-history-tab.component.html',
  styleUrls: ['./chat-history-tab.component.scss'],
  providers: [GetChatsHistoryService]
})
export class ChatHistoryTabComponent implements OnInit {

  constructor(private _chosenChatService: PassChosenChatIdService, private _GetChatsHistoryService: GetChatsHistoryService, private globalVaribles: GlobalVariblesService) { }
  chosenChat:number = -1;
  chosenChatData = undefined;
  userFullName:string;
  agentImg = "assets/agent.png";
  userImg = "assets/user.png";
  inspectHistory = false;
  baseUrl:string = environment.baseUrl;
 
  ngOnInit()
  {
     this._chosenChatService.currChosenChatID.subscribe(chosenChatID=>this.requestChatDataByChatID(chosenChatID));     
     this.globalVaribles.filterHistory.subscribe(x=>this.inspectHistory = false);
  }
  requestChatDataByChatID(chosenChatID):void
  {
    this.inspectHistory = true;
    this.chosenChat = chosenChatID;
    
    //when the value is -1 it means the user didnt press a chat
    // - the function was called on the first cycle of "on init"
    if(this.chosenChat == -1)
    {
      return;
    }
    this._GetChatsHistoryService.getChatById(chosenChatID)
                                 .subscribe(chatMessages=>this.requestNameByUserID(chosenChatID, chatMessages));                        
  }
  requestNameByUserID(userID, chatMessages):void
  {
    this.chosenChatData = chatMessages;//we have to save answer..
    this._GetChatsHistoryService.getUserFullNameByUserID(userID)
                                .subscribe(userFullName=>this.userFullName = userFullName.FirstName + ' ' + userFullName.LastName);                       
  }
  handleDownloadClick():void
  {    
    let chatHistoryDownloadLink = "http://" + this.baseUrl +"/api/chat/history/downloadSpecificChatForAgent/"+this.chosenChat;    
    window.open(chatHistoryDownloadLink, 'Download', '_blank').focus();
  }
}
