import { TestBed, inject } from '@angular/core/testing';

import { PassChosenChatIdService } from './pass-chosen-chat-id.service';

describe('PassChosenChatIdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PassChosenChatIdService]
    });
  });

  it('should be created', inject([PassChosenChatIdService], (service: PassChosenChatIdService) => {
    expect(service).toBeTruthy();
  }));
});
