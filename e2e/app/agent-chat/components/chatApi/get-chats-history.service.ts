import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';

import { Message } from '../../shared/models/message-model';
import { HistoryChatDetails } from '../../shared/models/history-chat-details-model';
import { userFullName } from '../../shared/models/user-full-name-model';


@Injectable()
export class GetChatsHistoryService {

  constructor(private http: HttpClient) { }
  requestChats():Observable<HistoryChatDetails[]>
  {
    const uri = 'http://localhost:62752/api/chats';
    return this.http.get<HistoryChatDetails[]>(uri);
  }
  getChatById(chatID: number):Observable<Message[]>
  {
    const uri = 'http://localhost:62752/api/chat/history/'+chatID;    
    return this.http.get<Message[]>(uri);
  }
  getUserFullNameByUserID(userID: number):Observable<userFullName>
  {
    const uri = 'http://localhost:62752/api/users/get_name_by_id/'+userID;
    return this.http.get<userFullName>(uri);
  }
  requestFilteredChats(date:string, branch: string):Observable<HistoryChatDetails[]>
  {
    const uri = 'http://localhost:62752/api/chat/history/filter/'+date+'/'+branch;
    return this.http.get<HistoryChatDetails[]>(uri);
  }
  requestChatRaiting(chatID:number):Observable<number>
  {
    const uri = 'http://localhost:62752/api/chat/getRaiting/'+chatID;
    return this.http.get<number>(uri);
  }
  
}
