import { TestBed, inject } from '@angular/core/testing';

import { GetCannedMessagesService } from './get-canned-messages.service';

describe('GetCannedMessagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetCannedMessagesService]
    });
  });

  it('should be created', inject([GetCannedMessagesService], (service: GetCannedMessagesService) => {
    expect(service).toBeTruthy();
  }));
});
