export interface HistoryChatDetails
{
  userFirstName:string;
  userLastName:string;
  userId:number;
  agentId:number;
  branch:string;
  raiting?:number;
  chatId:number;
}
