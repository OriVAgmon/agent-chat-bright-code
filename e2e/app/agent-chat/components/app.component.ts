import { Component, Output, OnInit, Input, EventEmitter } from '@angular/core';
import { GlobalVariblesService } from './chatApi/global-varibles.service';
import { notificationToHandle } from './chatApi/notificationToHandle';
import { GetAllUsersService } from './chatApi/get-all-users.service';
import { SingalRService } from './singal-r.service';
import { GetUserIdService } from './chatApi/get-user-id.service';


import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @Output() event: EventEmitter<any> = new EventEmitter();
  @Input()

  title = 'app';
  chatTab = false;
  agentsChatTab = false;
  cannedMessagesTab = false;
  historyTab = false;
  chatType;
  currAgentID=1;//should be depending on current agent identity
  color='black';
  countNotifications = 0;
  public isChatActive = false;
  public _chosenUser:string;
  public openMiddleChat(chosenUser)
  {
    this.isChatActive = true;
    this._chosenUser=chosenUser;
    //pass value to profile & chat components
  }
  ngOnInit()
  {
    this.globalVaribles.notificationListToHandle.subscribe(x=>{this.handleNewNotification(x)});
    //this.globalVaribles.gotTransferedUser.subscribe(x=>{this.handleTransferedChat(x)});
  }
  constructor(private globalVaribles : GlobalVariblesService, private getAllUsers : GetAllUsersService, private signalr:SingalRService, private getUserId: GetUserIdService, private http: Http)
  {

  }
  handleNewNotification(notificationListToHandle)
  {

    if(!notificationListToHandle || notificationListToHandle == -1 || Object.keys(notificationListToHandle).length === 0 && notificationListToHandle.constructor === Object  )
    {
      return;
    }
    if(this.globalVaribles.notificationsToHandle.length > this.countNotifications)//if the call to this function was made bevause a new notification was pushed
    {
      this.color = 'royalblue';
      this.countNotifications++;      
    }
  }

  clickedNotificationButton()
  {

    this.countNotifications--;
    if(this.countNotifications >=0)//it means there is atleast one notification we have to handle!
    {
      this.handleNotification(this.globalVaribles.notificationListToHandle.value[0]);
    }

    if(this.countNotifications <= 0)//not letting the counter be less then 0.
    {
      this.countNotifications = 0;
      this.color = 'black';                  
    }


  }

  handleNotification(notificationToHandle:notificationToHandle)
  {
    if(notificationToHandle.isAgent)//that's a message from internalChat
    {
      this.getAllUsers.requestAgentProfile(notificationToHandle.clientID).subscribe(agent=>{this.openUpAgentsChat(agent)}); 
      this.globalVaribles.popNotificationThatHandled();//pop after handle is complete!
           
    }
    else//that's a new/ transfered user from internal chat.
    {      
      this.getAllUsers.requestUserProfile(notificationToHandle.clientID).subscribe(user=>{this.openUpClientsChat(user)});
      this.globalVaribles.popNotificationThatHandled();//pop after handle is complete!
      this.signalr.isReceiverAgent = false;
      this.getUserId.userID = notificationToHandle.clientID;
      this.signalr.registerConnection();
    }
  }
  openUpAgentsChat(agent)
  {
    if(agent != null)
    {
      this._chosenUser = agent;
    }
    this.agentsChatTab = true;
    this.isChatActive = true;
    this.chatTab = false;
    this.cannedMessagesTab = false;
    this.historyTab = false;
    this.chatType='Agents Chat';

  }

  openUpClientsChat(user)
  {
    if(user != null)
    {
      this._chosenUser = user;
    }
    this.isChatActive = true;
    this.chatTab = true;
    this.agentsChatTab = false;
    this.cannedMessagesTab = false;
    this.historyTab = false;
    this.chatType='Clients Chat';
  }
  debugSQL()
  {
    var uri = 'http://localhost:62752/api/chat/debugSQL';
    return this.http.get(uri);
  }

}

