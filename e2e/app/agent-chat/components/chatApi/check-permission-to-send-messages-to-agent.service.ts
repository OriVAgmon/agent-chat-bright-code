import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CheckPermissionToSendMessagesToAgentService {

  constructor(private http: HttpClient) { }
  checkPermissionToSendMessages(agentID:number, chatID:number):Observable<boolean>
  {
    const uri = "http://localhost:62752/api/chat/isAgentHasPermissionToSendMessages/" + chatID + '/' + agentID;
    return this.http.get<boolean>(uri)
  }
}
