import { Injectable, VERSION, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class PassChosenChatIdService {

  private chosenChatID = new BehaviorSubject<number>(-1);
  currChosenChatID = this.chosenChatID.asObservable();
  constructor() { }
  changeCurrChosenChatID(currChatID: number)
  {
    this.chosenChatID.next(currChatID);
  }
}
//to check: why the hell I the subscribing doesnt work in chat-history-tab