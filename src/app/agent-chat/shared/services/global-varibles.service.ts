import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { notificationToHandle } from './notificationToHandle';
import { GetAgentIdService } from './get-agent-id.service';
import { GetUserIdService } from './get-user-id.service';

@Injectable()
export class GlobalVariblesService {

  constructor(private getUserIdService:GetUserIdService, private getAgentId:GetAgentIdService) { }
  chatID = new BehaviorSubject<number>(-1);
  chatIDAsObservable = this.chatID.asObservable();
  oldChatID:number;
  filteredChatHistory = new BehaviorSubject<object>(null);
  filteredChatHistoryAsObservable = this.filteredChatHistory.asObservable();
  isRegConn = false;
  ourAgentID;

  //might contain a bug if two users are removed/ added simultaneously.
  newUser = new BehaviorSubject<object>(null);
  newUserAsObservable = this.newUser.asObservable();

  userInActive = new BehaviorSubject<number>(null);
  userInActiveAsObservable = this.userInActive.asObservable();

  shouldRemoveInActiveUser = new BehaviorSubject<number>(null);
  shouldRemoveInActiveUserAsObservable = this.shouldRemoveInActiveUser.asObservable();

  userInActiveWasReconnected = new BehaviorSubject<number>(null);
  userInActiveWasReconnectedAsObservable = this.userInActiveWasReconnected.asObservable();

  agentState = new BehaviorSubject<string>(null);
  agentStateAsObservable = this.agentState.asObservable();
  agentIDToChangeState = new BehaviorSubject<number>(null);
  agentIDToChangeStateAsObservable = this.agentIDToChangeState.asObservable();

  userIDToUpdateIP = new BehaviorSubject<number>(null);
  userIDToUpdateIPAsObservable = this.userIDToUpdateIP.asObservable();
  userIPToUpdate = new BehaviorSubject<string>(null);
  userIPToUpdatesObservable = this.userIPToUpdate.asObservable();
  userLocationToUpdate = new BehaviorSubject<string>(null);
  userLocationToUpdateObservable = this.userLocationToUpdate.asObservable();

  filterHistory = new BehaviorSubject<number>(0);//will be changed every time a req to filter history is made
  
  //users transfer
  gotTransferedUser = new BehaviorSubject<number>(-1);//will be changed to transfered userID when user is transfered to curr agent.
  weJustTransferedAUser = new BehaviorSubject<number>(-1);//will be changed to transfered userID when we  transfered user to other agent

  //handeling notifications
  notificationsToHandle = new Array <notificationToHandle>();
  notificationListToHandle = new BehaviorSubject<notificationToHandle[]>(null);

  //updating chat data
  isAgentsChat = new BehaviorSubject<boolean>(null);

  setUserID(cID:number):void{
      this.oldChatID = this.chatID.getValue();
      this.chatID.next(cID);
  }
  updateIpUserIDAndLocation(ip:string, location:string, chosenUserID:number):void{
    this.userLocationToUpdate.next(location);    
    this.userIPToUpdate.next(ip);
    this.userIDToUpdateIP.next(chosenUserID);
  }
  pushNewNotificationToHandle(clientID:number, isAgent:boolean):void{
    console.log(clientID);
    for(let i = 0; i < this.notificationsToHandle.length; i++)
    {
      var element = this.notificationListToHandle.value[i];
      if(element == undefined)
      {
        continue;
      }
      if(element.clientID == clientID && isAgent == this.notificationListToHandle.value[i].isAgent)
      {
        return;//we dont want to add that notification because we already added it.
      }

    }
    this.playNotificationSound();
    this.notificationsToHandle.push( new notificationToHandle(clientID, isAgent));//pushing the new notification to the array
    this.notificationListToHandle.next(Object.assign({}, this.notificationsToHandle));//pushing the array to the observable
  }
  popNotificationThatHandled():void{
    this.notificationsToHandle.shift();//pushing the new notification to the array
    this.notificationListToHandle.next(Object.assign({}, this.notificationsToHandle));//pushing the array to the observable
  }
  playNotificationSound():void{
    var audio = new Audio();
    audio.src = "assets/newUserNoticifation.wav";
    audio.load();
    audio.play();
  }
}
