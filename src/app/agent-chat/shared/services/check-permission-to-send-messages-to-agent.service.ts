import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CheckPermissionToSendMessagesToAgentService {

  constructor(private http: HttpClient) { }
  baseUrl:string = environment.baseUrl;
  checkPermissionToSendMessages(agentID:number, chatID:number):Observable<boolean>{
    const uri = "http://" + this.baseUrl + "/api/chat/isAgentHasPermissionToSendMessages/" + chatID + '/' + agentID;
    return this.http.get<boolean>(uri)
  }
}
