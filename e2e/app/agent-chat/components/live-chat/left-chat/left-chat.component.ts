import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GetAllUsersService } from '../../chatApi/get-all-users.service';
import { GetAgentIdService } from './../../chatApi/get-agent-id.service';
import { GetUserIdService } from './../../chatApi/get-user-id.service';
import { GlobalVariblesService } from './../../chatApi/global-varibles.service';
import { SingalRService } from '../../singal-r.service';
import 'rxjs/add/operator/map';  

import { User } from '../../../shared/models/user-model';

@Component({
  selector: 'app-left-chat',
  templateUrl: './left-chat.component.html',
  styleUrls: ['./left-chat.component.scss'],
  providers:[GetAllUsersService]
})
export class LeftChatComponent implements OnInit {
@Output() event: EventEmitter<any> = new EventEmitter();
@Input()


chatType:string;
allUsers:User[] = [];
colors=['green', 'black', 'black', 'black'];// status/ state colors
chosenUserColors = ['black', 'black', 'black', 'black'];
lastIndexState = -1;
lastIndexUser = -1;
currAgentID = 1;
userUpdatedWithIP;
statesToCssClasses = {'online':'circle', 'offline':'circleInActive', 'busy':'busyCircle', 'invisible':'circleInActive'};


constructor(
  private allUsersService :GetAllUsersService,
  private getAgentIdService:GetAgentIdService,
  private getUserIdService:GetUserIdService,
  private globalVaribles: GlobalVariblesService,
  private signalR:SingalRService)
  {
    allUsersService.requestAllUsers(this.globalVaribles.ourAgentID).subscribe(allUsers => this.allUsers=allUsers);
  }
  ngOnInit()
  {
    console.log("Should print this");
    this.globalVaribles.newUserAsObservable.subscribe(newUser=>this.addNewUser(newUser));
    this.globalVaribles.userInActiveAsObservable.subscribe(uidToRemove=>this.handleUserInActive(uidToRemove));
    this.globalVaribles.shouldRemoveInActiveUserAsObservable.subscribe( uid=>this.removeInActiveUserFromList(uid));
    this.globalVaribles.userInActiveWasReconnectedAsObservable.subscribe( uid=>this.handleReconnectedUserInActive(uid));
  }
  handleUserInActive(uid:number)
  {
    if(uid == null || document.getElementById( uid.toString() ) == null)
    {
      return;
    }
    document.getElementById( uid.toString() ).className="circleInActive";
  }
  handleReconnectedUserInActive(uid:number)
  {
    if(uid == null)
    {
      return;
    }
    if(document.getElementById( uid.toString() ) == null)
    {
      return;
    }
    document.getElementById( uid.toString() ).className="circle";
    this.globalVaribles.userInActiveWasReconnected.next(null);//reset value
  }
  removeInActiveUserFromList(uid)
  {
    if(uid == null)
    {
      return;
    }
    let indexToRemove = -1;
    for(let i = 0; this.allUsers && i < this.allUsers.length; i++)//seek for the index we want to remove.
    {
        if(this.allUsers[i].userID == uid)
        {
          indexToRemove = i;
          break;  
        }
    }
    if (indexToRemove != -1)//remove relvant user we need to remove.
    {
      this.allUsers.splice(indexToRemove, 1);
      this.globalVaribles.shouldRemoveInActiveUser.next(null);//reset
    }
  }
  
  addNewUser(newUser)
  {
    if(newUser == null)
    {
      return;
    }
    //checking if the user is already conncted and he is just reconnecting now
    if(this.allUsers != undefined)
    {
      for(let i = 0; i < this.allUsers.length; i++)
      {
        if(this.allUsers[i].email == newUser.userEmail)
        {
          return;
        }
      }
    }
   

    this.allUsers = [...this.allUsers, newUser];//push and solve detection problem
  }
  
  changeAgentState(index)
  {
    this.colors[0] = 'black';//reseting the array.    
    this.colors[index] = 'green';
    let state:string;
    if(this.lastIndexState != -1)
      this.colors[this.lastIndexState] = 'black';
    this.lastIndexState = index;
    if(index == 0)
    {
      state = "online";
    }
    else if(index == 1)
    {
      state = "busy";
    }
    else if(index == 2)
    {
      state = "invisible";
    }
    else if(index == 3)
    {
      state = "offline";
    }
    else
    {
      return;
    }
    this.signalR.updateCurrAgent(this.globalVaribles.ourAgentID, state);
  }

  chat(user, index)//this function colors chosen chat, and notify other components a user was chosen.
  {
    
    //switch color to user
    this.chosenUserColors[index] = 'royalblue';
    if(this.lastIndexUser != -1)
      this.chosenUserColors[this.lastIndexUser] = 'black';
    this.lastIndexUser = index;
    //Throw event to parent so we open chat section:
    this.getUserIdService.userID = user.userID;
    this.globalVaribles.setUserID(user.userID);
    this.allUsersService.requestUserProfile(this.getUserIdService.userID).subscribe(profile=>this.onGettingUpdatedUserProfile(profile, user));
    
  }

  onGettingUpdatedUserProfile(profile, user)
  {
    var userWithIP = {FirstName:user.FirstName, LastName:user.LastName, branch:user.branch,
                  currTime:user.currTime, userEmail:user.userEmail, userID:user.userID, ip_address:profile.ip_address,
                    website_history:user.website_history, user_location: profile.user_location};  

    this.event.emit(userWithIP);
  }
}
