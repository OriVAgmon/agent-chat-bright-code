import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';  
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {FileUploadModule} from 'primeng/primeng';


import * as $ from 'jquery';

import {
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSidenavModule,
  MatSliderModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';


import { AppComponent } from './app.component';
import { LeftChatComponent } from './live-chat/left-chat/left-chat.component';
import { MiddleChatComponent } from './live-chat/middle-chat/middle-chat.component';
import { RightChatComponent } from './live-chat/right-chat/right-chat.component';
import { LeftChatAgentsComponent } from './live-chat/agents-chat/left-chat-agents/left-chat-agents.component';
import { MiddleChatAgentsComponent } from './live-chat/agents-chat/middle-chat-agents/middle-chat-agents.component';
import { RightChatAgentsComponent } from './live-chat/agents-chat/right-chat-agents/right-chat-agents.component';
import { MessageService } from './message.service';
import { ChatHistoryTabComponent } from './chat-history/chat-history-tab/chat-history-tab.component';
import { FilterHistoryComponent } from './chat-history/filter-history/filter-history.component';
import { ChatSessionsComponent } from './chat-history/chat-sessions/chat-sessions.component';
import { CannedMessagesComponent } from './canned-messages/canned-messages/canned-messages.component';
import { FilterMessagesComponent } from './canned-messages/filter-messages/filter-messages.component';
import { SearchMessagesComponent } from './canned-messages/search-messages/search-messages.component';
import { HttpClient } from '@angular/common/http';
import { PassChosenChatIdService } from './chat-history/pass-chosen-chat-id.service'
import { GetAllUsersService } from './chatApi/get-all-users.service'
import { GetAgentIdService } from './chatApi/get-agent-id.service'
import { GetUserIdService } from './chatApi/get-user-id.service'
import { GlobalVariblesService } from './chatApi/global-varibles.service'
import { GetChatsHistoryService } from './chatApi/get-chats-history.service'
import { SingalRService } from './singal-r.service'




@NgModule({
  declarations: [
    AppComponent,
    LeftChatComponent,
    MiddleChatComponent,
    RightChatComponent,
    LeftChatAgentsComponent,
    MiddleChatAgentsComponent,
    RightChatAgentsComponent,
    ChatHistoryTabComponent,
    FilterHistoryComponent,
    ChatSessionsComponent,
    CannedMessagesComponent,
    FilterMessagesComponent,
    SearchMessagesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FileUploadModule,
    HttpModule,
    HttpClientModule,
  ],
  exports: [MatButtonModule,
            MatCheckboxModule,
            MatSelectModule,
            MatAutocompleteModule,
            MatButtonModule,
            MatButtonToggleModule,
            MatCardModule,
            MatCheckboxModule,
            MatChipsModule,
            MatStepperModule,
            MatDatepickerModule,
            MatDialogModule,
            MatDividerModule,
            MatExpansionModule,
            MatGridListModule,
            MatIconModule,
            MatInputModule,
            MatListModule,
            MatMenuModule,
            MatNativeDateModule,
            MatPaginatorModule,
            MatProgressBarModule,
            MatProgressSpinnerModule,
            MatRadioModule,
            MatRippleModule,
            MatSelectModule,
            MatSidenavModule,
            MatSliderModule,
            MatSlideToggleModule,
            MatSnackBarModule,
            MatSortModule,
            MatTableModule,
            MatTabsModule,
            MatToolbarModule,
            MatTooltipModule,
          ],
  providers: [MessageService, PassChosenChatIdService, GetAllUsersService, GetAgentIdService, GetUserIdService, GlobalVariblesService, GetChatsHistoryService, SingalRService],
  bootstrap: [AppComponent],
})
export class AppModule { }
