import { AgentChatModule } from './agent-chat.module';

describe('AgentChatModule', () => {
  let agentChatModule: AgentChatModule;

  beforeEach(() => {
    agentChatModule = new AgentChatModule();
  });

  it('should create an instance', () => {
    expect(agentChatModule).toBeTruthy();
  });
});
