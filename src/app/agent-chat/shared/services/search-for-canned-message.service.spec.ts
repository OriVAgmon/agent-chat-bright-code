import { TestBed, inject } from '@angular/core/testing';

import { SearchForCannedMessageService } from './search-for-canned-message.service';

describe('SearchForCannedMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchForCannedMessageService]
    });
  });

  it('should be created', inject([SearchForCannedMessageService], (service: SearchForCannedMessageService) => {
    expect(service).toBeTruthy();
  }));
});
