import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { CannedMessage } from './../models/canned-message-model';
import { environment } from './../../../../environments/environment';

@Injectable()
export class AllCannedMessagesService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getAllCannedMessagesOfAgentById(agentID: number): Observable<CannedMessage[]> {
    const uri =  `http://${this.baseUrl}/api/cannedmessages/${agentID}`;
    return this.http.get<CannedMessage[]>(uri);
  }

}

