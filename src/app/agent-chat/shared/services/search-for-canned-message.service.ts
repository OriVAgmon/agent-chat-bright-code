import { Injectable } from '@angular/core';
import { Response } from '@angular/http';  
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  

import { CannedMessage } from './../../shared/models/canned-message-model';
import { environment } from '../../../../environments/environment';


@Injectable()
export class SearchForCannedMessageService {

  constructor(private http: HttpClient) { }

  baseUrl:string = environment.baseUrl;

  searchForCannedMessage(agentID:number, searchInput:any):Observable<CannedMessage[]>{    
    const uri = "http://" + this.baseUrl + "/api/cannedmessages/search/"+agentID+'/'+searchInput;
    return this.http.get<CannedMessage[]>(uri);
  }
}

