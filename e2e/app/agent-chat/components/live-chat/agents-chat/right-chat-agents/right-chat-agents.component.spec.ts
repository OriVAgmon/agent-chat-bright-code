import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightChatAgentsComponent } from './right-chat-agents.component';

describe('RightChatComponent', () => {
  let component: RightChatAgentsComponent;
  let fixture: ComponentFixture<RightChatAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightChatAgentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightChatAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
