import { Component, OnInit } from '@angular/core';
import { SearchForCannedMessageService } from '../../../shared/services/search-for-canned-message.service'
import { CannedMessage } from './../../../shared/models/canned-message-model';

@Component({
  selector: 'app-search-messages',
  templateUrl: './search-messages.component.html',
  styleUrls: ['./search-messages.component.scss'],
  providers:[SearchForCannedMessageService]
})
export class SearchMessagesComponent implements OnInit {

  constructor(private SearchForCannedMessageService: SearchForCannedMessageService) { }

  ngOnInit()
  {
  }
  agentID:number = 1;
  searchInput:string;
  searchOutput = false;
  searchResults:CannedMessage[];
  handleSearchClick():void
  {
    this.SearchForCannedMessageService.searchForCannedMessage(this.agentID, this.searchInput)
                                                              .subscribe(searchRes=>this.searchResults=searchRes);
    this.searchOutput  = !this.searchOutput;
  }
}
