import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatSessionsComponent } from './chat-sessions.component';

describe('ChatSessionsComponent', () => {
  let component: ChatSessionsComponent;
  let fixture: ComponentFixture<ChatSessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatSessionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatSessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
