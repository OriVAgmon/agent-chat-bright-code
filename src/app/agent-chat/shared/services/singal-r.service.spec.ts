import { TestBed, inject } from '@angular/core/testing';

import { SingalRService } from './singal-r.service';

describe('SingalRService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SingalRService]
    });
  });

  it('should be created', inject([SingalRService], (service: SingalRService) => {
    expect(service).toBeTruthy();
  }));
});
