import { Component, OnInit, Input } from '@angular/core';
import { GetCannedMessagesService } from '../../chatApi/get-canned-messages.service';
import { GlobalVariblesService } from '../../chatApi/global-varibles.service';

import { CannedMessage } from './../../../shared/models/canned-message-model';


@Component({
  selector: 'app-canned-messages',
  templateUrl: './canned-messages.component.html',
  styleUrls: ['./canned-messages.component.scss'],
  providers:[GetCannedMessagesService]
})
export class CannedMessagesComponent implements OnInit {
  constructor(private cannedMessages:GetCannedMessagesService, private globalVaribles: GlobalVariblesService) {
  }
  ngOnInit() {    
    this.cannedMessages.requestAllCannedMessages(this.globalVaribles.ourAgentID)
    .subscribe(cannedMessages=>this.allCannedMessagesOfCurrentAgent = cannedMessages);
    for(let i=0; i < this.allCannedMessagesOfCurrentAgent.length; i++)
    {
      this.editRow[i] = false;
    }

  }
  allCannedMessagesOfCurrentAgent:CannedMessage[];

  //varibles for editing messages:
  editRow=[];
  editMessageBox = false;

  //varibles for setting a new message:
  addMessage = false;
  newlyCreatedMessageName:string;
  newlyCreatedMessageContent:string;
  newlyCreatedMessageID=-1;
  
  //varibles for editing an existing message:
  editMessage = false;
  updatedMessageName:string;
  updatedMessageContent:string;
  messageIDToUpdate=-1;

  //varibles for deleting an existing message:
  messageIDToDelete=-1;

  mouseEnter(index)
  {
    this.editRow[index] = true;
  }
  mouseLeave(index)
  {
    this.editRow[index] = false;
  }
  handleEditMessage()
  {
    //get index of message:
    for(let i = 0; i < this.allCannedMessagesOfCurrentAgent.length; i++)
    {
      if(this.allCannedMessagesOfCurrentAgent[i].canned_messageID == this.messageIDToUpdate)
      {
        this.allCannedMessagesOfCurrentAgent[i].name = this.updatedMessageName;
        this.allCannedMessagesOfCurrentAgent[i].content = this.updatedMessageContent;
        break;
      }
    }

    this.cannedMessages.requestEditCannedMessage(this.messageIDToUpdate, this.globalVaribles.ourAgentID, this.updatedMessageContent, this.updatedMessageName);
    this.editMessageBox = false;
  }
  handleAddCannedMessage()
  {
    //query post to DB.
    this.cannedMessages.requestAddCannedMessage(this.globalVaribles.ourAgentID,
                                                this.newlyCreatedMessageName,
                                                this.newlyCreatedMessageContent)
                                                .subscribe(updatedCannedMessages=>this.allCannedMessagesOfCurrentAgent = updatedCannedMessages);
                                                         
    this.addMessage=false;
  }
  handleDeleteCannedMessage()
  {
    //query post to DB.
    this.cannedMessages.requestDeleteCannedMessage(this.globalVaribles.ourAgentID, this.messageIDToDelete)
    .subscribe(updatedCannedMessages=>this.allCannedMessagesOfCurrentAgent = updatedCannedMessages);
  }
}
