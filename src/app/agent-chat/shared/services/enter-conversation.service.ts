import { Injectable } from '@angular/core';
import { Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';

@Injectable()
export class EnterConversationService {

  constructor(private http: HttpClient) { }
  baseUrl:string = environment.baseUrl;
  //this function is returning message history & chatID AND creating a new chat in chat table.
  conversationStart(chosenUserFromQueue, agentID:number):Observable<any>{
    //get history chat with curr user (if he is a new user we will get empty string):
    if(chosenUserFromQueue.branch == undefined)
    {
      chosenUserFromQueue.branch = "undefined";
    }
    const uri = "http://" + this.baseUrl + "/api/chat/history/"+chosenUserFromQueue.userID+'/'+agentID+'/'+chosenUserFromQueue.branch;
    return this.http.get<any>(uri);
  }
  //this function returning message history AND creating a new chat in chat table.
  conversationStartAgents(chosenAgent, agentID:number):Observable<any>{
    //get history chat with curr user (if he is a new user we will get empty string):
    const uri = "http://" + this.baseUrl + "/api/chat/history/"+chosenAgent.agentID+'/'+agentID;
    
    //returning meesage history
    return this.http.get<any>(uri)
  }
}
