import { Component, OnInit, NgModule, VERSION, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms';



import { GlobalVariblesService } from './../../../shared/services/global-varibles.service'
import { GetChatsHistoryService } from './../../../shared/services/get-chats-history.service'
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-filter-history',
  templateUrl: './filter-history.component.html',
  styleUrls: ['./filter-history.component.scss']
})
export class FilterHistoryComponent implements OnInit {

  constructor(private globalVariblesService:GlobalVariblesService, private getChatsHistoryService:GetChatsHistoryService) { }
  dateSelected:string='Date Range';
  branchSelected:string='Branch';
  customRange:string[];
  
  ngOnInit()
  {
  }
  ngOnChanges(changes: SimpleChanges)
  {
    for(let propName in changes)
    {
      let chng = changes[propName];
      let cur  = JSON.stringify(chng.currentValue);    
      if(cur == 'Custom range')
      {
        console.log("selected");
      }
      
    }
  }
  public time="time";
  public branch="branch";
  public isCustomRange:boolean;
  applyFilter()
  {
    this.time = this.dateSelected;
    this.branch = this.branchSelected;
    this.isCustomRange = false;
    if(this.dateSelected == 'Custom range')//custom range was chosen
    {      
      this.isCustomRange = true;
      this.time = `${this.convertToSqlDate(this.customRange[0])}@${this.convertToSqlDate(this.customRange[1])}`;
    }    
    //do query and update session component
    this.getChatsHistoryService.requestFilteredChats(this.time, this.branch, this.isCustomRange)
                                .subscribe
                                (filteredChatHistory=>this.globalVariblesService.filteredChatHistory.next(filteredChatHistory));
    if(this.time != "time" || this.branch != "branch")
    {
      this.globalVariblesService.filterHistory.next( this.globalVariblesService.filterHistory.value + 1);
    }
    this.dateSelected = "Date";
  }
  convertToSqlDate(jsDate:string):string{
    let stringAsdate = new Date(jsDate);
    return (stringAsdate.getUTCFullYear()) + '-' + (stringAsdate.getUTCMonth() + 1) + '-' + (stringAsdate.getUTCDate());
  }

}
