export interface User {
  email:string;
  currTime?:Date;
  FirstName:string;
  LastName:string;
  branch?:string;
  userID:number;
  userLocation:string;
  userIp:string;
  websiteHistory:string;
  isCurrentlyActive?:boolean;
  stickyManagerID?:number;
}
