import { Component, OnInit, ElementRef } from '@angular/core';
import { GetChatsHistoryService } from '../../../shared/services/get-chats-history.service';
import { PassChosenChatIdService } from '../pass-chosen-chat-id.service';
import { GlobalVariblesService } from './../../../shared/services/global-varibles.service'

declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-chat-sessions',
  templateUrl: './chat-sessions.component.html',
  styleUrls: ['./chat-sessions.component.scss'],
  providers:[GetChatsHistoryService]
})

export class ChatSessionsComponent implements OnInit {

  constructor(private _chatsHistoryService: GetChatsHistoryService, private _chosenChatService: PassChosenChatIdService, private globalVariblesService:GlobalVariblesService)
  { 
    this._chatsHistoryService.requestChats()
          .subscribe(res=>this.populateFullNames(res));
  }

  
  ngOnInit()
  {
    this.globalVariblesService.filteredChatHistoryAsObservable.subscribe(res=>this.populateFullNames(res));    
  }

  chatsHistory;
  usersFullNames = [];
  chosenChat:number;
  isHover:boolean[]=[];
  isAllChatSessionDataPopulated:boolean = false;
  
  mouseEnter(index):void
  {
    this.isHover[index] = true;
  }

  mouseLeave(index):void
  {
    this.isHover[index] = false;
  }

  openHistory(userID, agentID, firstName, chatID):void
  {
    this._chosenChatService.changeCurrChosenChatID(chatID);

  }
  populateFullNames(res):void
  {
    this.chatsHistory = res;
    const fullStar = "assets/glyphicons-50-star.png";  
    const emptyStar = "assets/glyphicons-49-star-empty.png";  
    if(this.chatsHistory == undefined )
    {
      return;
    }
    for(let i = 0; i < this.chatsHistory.length; i++)
    {
      if(this.chatsHistory[i].raiting == 1)
      {
        this.chatsHistory[i].raiting = [fullStar, emptyStar, emptyStar, emptyStar, emptyStar];
      }
      else if(this.chatsHistory[i].raiting == 2)
      {
        this.chatsHistory[i].raiting = [fullStar, fullStar, emptyStar, emptyStar, emptyStar];        
      }
      else if(this.chatsHistory[i].raiting == 3)
      {
        this.chatsHistory[i].raiting = [fullStar, fullStar, fullStar, emptyStar, emptyStar];        
      }
      else if(this.chatsHistory[i].raiting == 4)
      {
        this.chatsHistory[i].raiting = [fullStar, fullStar, fullStar, fullStar, emptyStar];        
      }
      else if(this.chatsHistory[i].raiting == 5)
      {
        this.chatsHistory[i].raiting = [fullStar, fullStar, fullStar, fullStar, fullStar];        
      }
    }
    this.isAllChatSessionDataPopulated= true;  
  }
  

}
