import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';

import { User } from '../../shared/models/user-model';
import { Agent } from '../../shared/models/agent-model';

import 'rxjs/add/operator/map';  
@Injectable()
export class GetAllUsersService {

  constructor(private http: HttpClient) { 
    
  }
  allUsers=[];
  savedAgentID = -1;

  //Return a list of all relevant users for curr agent to chat with.
  requestAllUsers(agentID:number):Observable<User[]>//return all relevant users for a queue
  {
    this.savedAgentID = agentID;
    const uri = 'http://localhost:62752/api/getRelevantUsers/'+agentID;
    return this.http.get<User[]>(uri);
  }

  requestAllAgents():Observable<Agent[]>
  {
    const uri = 'http://localhost:62752/api/agents';
    return this.http.get<Agent[]>(uri)  
  }

  allUsersDataset():User[]
  {  
    this.requestAllUsers(this.savedAgentID).subscribe( users =>this.allUsers=users );
    return this.allUsers;  
  }
  requestAllChatsOfAgent():Observable<Agent[]>
  {
    const uri = 'http://localhost:62752/api/users';
    return this.http.get<Agent[]>(uri);
  }  
  
  requestUserProfile(userID:number):Observable<User>
  {
    const uri = 'http://localhost:62752/api/users/'+userID;
    return this.http.get<User>(uri);
  }

  requestAgentProfile(agentID:number):Observable<Agent>
  {
    const uri = 'http://localhost:62752/api/agents/'+agentID;
    return this.http.get<Agent>(uri);
  }
  stickAgentToUser(agentID:number, userID:number):Observable<boolean>
  {
    const uri = 'http://localhost:62752/api/agents/stickUserToAgent/'+userID + '/' +agentID;
    return this.http.get<boolean>(uri);
  }
}
