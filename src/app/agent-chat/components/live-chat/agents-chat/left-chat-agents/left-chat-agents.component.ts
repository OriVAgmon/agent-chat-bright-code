import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GetAllUsersService } from './../../../../shared/services/get-all-users.service';
import { GetAgentIdService } from './../../../../shared/services/get-agent-id.service';
import { GetUserIdService } from './../../../../shared/services/get-user-id.service';
import { GlobalVariblesService } from './../../../../shared/services/global-varibles.service'
import { SingalRService } from './../../../../shared/services/singal-r.service'
import 'rxjs/add/operator/map';  

import { Agent } from '../../../../shared/models/agent-model';

@Component({
  selector: 'app-left-chat-agents',
  templateUrl: './left-chat-agents.component.html',
  styleUrls: ['./left-chat-agents.component.scss'],
  providers:[GetAllUsersService]
})
export class LeftChatAgentsComponent implements OnInit {
@Output() event: EventEmitter<string> = new EventEmitter();
@Input()

chatType;
allAgents:Agent[];
colors:string[]=['green', 'black', 'black', 'black'];// status/ state colors
chosenUserColors:string[] = ['black', 'black', 'black', 'black'];
lastIndexState:number = -1;
lastIndexUser:number = -1;
statesToCssClasses = {'online':'circle', 'offline':'circleInActive', 'busy':'busyCircle', 'invisible':'circleInActive'};
agentID:number;
  constructor(private allUsersService :GetAllUsersService, private signalR:SingalRService, private globalVaribles: GlobalVariblesService){
    
    allUsersService.requestAllAgents().subscribe(res =>{  
      this.allAgents=res;
      for(let i = 0; i < this.allAgents.length; i++)
      {
        if(this.allAgents[i] == null)//remove nulls
        {
          this.allAgents.splice(i, 1);
        }
      }
    });
   }
  ngOnInit(){        
    //check if agents states are changing
    this.globalVaribles.agentIDToChangeStateAsObservable.subscribe(agentID=>this.changeStateOfAnAgentByAgentID(agentID));
  }
  connectAgent():void{
    this.signalR.agentID = this.agentID;
    this.globalVaribles.ourAgentID=this.agentID;
    this.signalR.startConn(true);
    
  }
  changeAgentState(index):void{//this function is making a request to the server to change state of an agent
    this.colors[0] = 'black';//reseting the array.
    this.colors[index] = 'green';
    let state:string;
    if(this.lastIndexState != -1)
      this.colors[this.lastIndexState] = 'black';
    this.lastIndexState = index;
    if(index == 0)
    {
      state = "online";
    }
    else if(index == 1)
    {
      state = "busy";
    }
    else if(index == 2)
    {
      state = "invisible";
    }
    else if(index == 3)
    {
      state = "offline";
    }
    else
    {
      return;
    }
    this.signalR.updateCurrAgent(this.globalVaribles.ourAgentID, state);
  }
  
  //This function is changing the state of an agent
  changeStateOfAnAgentByAgentID(agentIDToChange:number):void{
    console.log("changeStateOfAnAgentByAgentID");    
    if(this.allAgents == undefined)
    {
      return;
    }

    let agentIndexToChange = -1;
    for(let i = 0; i < this.allAgents.length; i++)
    {
      if(this.allAgents[i].agentId == agentIDToChange)
      {
        agentIndexToChange = i;
      }
    }
    if(agentIndexToChange != -1)
    {
      this.globalVaribles.agentStateAsObservable.subscribe(state=>this.allAgents[agentIndexToChange].agentAvailability = state);
    }
  }
  chat(agent, index):void{
    
    //switch color to user
    this.chosenUserColors[index] = 'royalblue';
    if(this.lastIndexUser != -1)
      this.chosenUserColors[this.lastIndexUser] = 'black';
    this.lastIndexUser = index;
    //Throw event to parent so we open chat section:
    this.event.emit(agent);
  }
}
