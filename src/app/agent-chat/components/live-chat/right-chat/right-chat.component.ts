import { Component, OnInit, Input } from '@angular/core';
import { GetAllUsersService } from '../../../shared/services/get-all-users.service';
import { GlobalVariblesService } from './../../../shared/services/global-varibles.service';
import { SingalRService } from '../../../shared/services/singal-r.service';

import { User } from '../../../shared/models/user-model';


@Component({
  selector: 'app-right-chat',
  templateUrl: './right-chat.component.html',
  styleUrls: ['./right-chat.component.scss'],
})
export class RightChatComponent implements OnInit {

  constructor(
    private getAgentsAndUsersService:GetAllUsersService,
    private globalVaribles: GlobalVariblesService,
    private SingalRService: SingalRService ) {
  }
  ourAgentID:number = -1;
  displayHistory:boolean=false;
  allAvailableAgents = [];
  isAgentClickedtransferChat:boolean=false;
  @Input() chosenUser: User;
  ngOnInit()
  {
    this.getAgentsAndUsersService.requestAllAgents().subscribe(agentsList=>this.handleAgentsList(agentsList, false));
    this.globalVaribles.userIDToUpdateIP.subscribe(uid=>this.checkIfWeNeedToUpdateIPAddress(uid));
    this.ourAgentID = this.globalVaribles.ourAgentID;
  }
  transferChat(destAgentIndex):void{
    this.SingalRService.transferUser(this.allAvailableAgents[destAgentIndex].agentID, this.chosenUser.userID);
  }

  handleAgentsList(agentsList, isClickedOnTransferAgentButton:boolean):void{
    if(isClickedOnTransferAgentButton)
    {
      this.isAgentClickedtransferChat = !this.isAgentClickedtransferChat;
    }
    this.allAvailableAgents=agentsList;
    
    for(let i = 0; i < this.allAvailableAgents.length;i++)//removing curr agent (we cant transfer a user to curr agent)
    {
      if(this.allAvailableAgents[i] != null && this.allAvailableAgents[i].agentID == this.globalVaribles.ourAgentID)//removing curr agent.
      {
        this.allAvailableAgents.splice(i, 1);
      }
    }
  }
  
  //this function excecutes whenever the global varible (userIDToUpdateIP) is being changed,
  //the function will update user id and location if userIDToUpdateIP is equal to the userID we are talking to.
  checkIfWeNeedToUpdateIPAddress(uid:number):void {
    if(this.chosenUser.userID == uid)
    {
      this.chosenUser.userIp = this.globalVaribles.userIPToUpdate.value;
      this.chosenUser.userLocation = this.globalVaribles.userLocationToUpdate.value;
    }
  }
  getUpdatedAgentsList():void{
    this.getAgentsAndUsersService.requestAllAgents().subscribe(agentsList=>this.handleAgentsList(agentsList, true));    
  }
  stickUserToCurrAgent():void{
    this.getAgentsAndUsersService.stickAgentToUser(this.globalVaribles.ourAgentID, this.chosenUser.userID).subscribe(nOp=>{void(0)});
  }
}

