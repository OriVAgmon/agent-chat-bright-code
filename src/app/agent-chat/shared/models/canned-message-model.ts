import { AgentTypes } from "../enums/agents-types";

export interface CannedMessage {
    cannedMessageAgentId?: number;
    name: string;
    content: string;
    canned_messageID: number;
    isAgentToAgentMessage: boolean;
}

