export class notificationToHandle
{
  clientID:number;
  isAgent:boolean;  
  constructor(clientID:number, isAgent:boolean){
      this.clientID = clientID;
      this.isAgent = isAgent;
  }
}