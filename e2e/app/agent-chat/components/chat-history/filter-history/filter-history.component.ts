import { Component, OnInit, NgModule, VERSION, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms';


import { GlobalVariblesService } from './../../chatApi/global-varibles.service'
import { GetChatsHistoryService } from './../../chatApi/get-chats-history.service'
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-filter-history',
  templateUrl: './filter-history.component.html',
  styleUrls: ['./filter-history.component.scss']
})
export class FilterHistoryComponent implements OnInit {

  constructor(private globalVariblesService:GlobalVariblesService, private getChatsHistoryService:GetChatsHistoryService) { }

  ngOnInit()
  {
  }
  ngOnChanges(changes: SimpleChanges)
  {
    for(let propName in changes)
    {
      let chng = changes[propName];
      let cur  = JSON.stringify(chng.currentValue);    
      if(cur == 'Custom range')
      {
      }
      
    }
  }
  public time="time";
  public branch="branch";
  applyFilter()
  {
    //do query and update session component
    this.getChatsHistoryService.requestFilteredChats(this.time, this.branch)
                                .subscribe
                                (filteredChatHistory=>this.globalVariblesService.filteredChatHistory.next(filteredChatHistory));
    if(this.time != "time" || this.branch != "branch")
    {
      this.globalVariblesService.filterHistory.next( this.globalVariblesService.filterHistory.value + 1);
    }
  }

}
