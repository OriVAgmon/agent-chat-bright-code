import { TestBed, inject } from '@angular/core/testing';

import { GlobalVariblesService } from './global-varibles.service';

describe('GlobalVariblesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalVariblesService]
    });
  });

  it('should be created', inject([GlobalVariblesService], (service: GlobalVariblesService) => {
    expect(service).toBeTruthy();
  }));
});
