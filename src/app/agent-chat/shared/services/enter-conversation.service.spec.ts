import { TestBed, inject } from '@angular/core/testing';

import { EnterConversationService } from './enter-conversation.service';

describe('EnterConversationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnterConversationService]
    });
  });

  it('should be created', inject([EnterConversationService], (service: EnterConversationService) => {
    expect(service).toBeTruthy();
  }));
});
