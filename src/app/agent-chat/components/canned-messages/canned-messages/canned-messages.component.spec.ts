import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CannedMessagesComponent } from './canned-messages.component';

describe('CannedMessagesComponent', () => {
  let component: CannedMessagesComponent;
  let fixture: ComponentFixture<CannedMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CannedMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CannedMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
