import { TestBed, inject } from '@angular/core/testing';

import { GetChatsHistoryService } from './get-chats-history.service';

describe('GetChatsHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetChatsHistoryService]
    });
  });

  it('should be created', inject([GetChatsHistoryService], (service: GetChatsHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
