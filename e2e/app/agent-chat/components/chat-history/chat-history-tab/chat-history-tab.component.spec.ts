import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatHistoryTabComponent } from './chat-history-tab.component';

describe('ChatHistoryTabComponent', () => {
  let component: ChatHistoryTabComponent;
  let fixture: ComponentFixture<ChatHistoryTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatHistoryTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatHistoryTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
