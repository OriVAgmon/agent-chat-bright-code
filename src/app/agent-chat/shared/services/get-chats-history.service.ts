import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';

import { Message } from '../../../agent-chat/shared/models/message-model';
import { HistoryChatDetails } from '../../../agent-chat/shared/models/history-chat-details-model';
import { userFullName } from '../../../agent-chat/shared/models/user-full-name-model';
import { environment } from '../../../../environments/environment';



@Injectable()
export class GetChatsHistoryService {
  baseUrl:string = environment.baseUrl;
  constructor(private http: HttpClient) { }
  requestChats():Observable<HistoryChatDetails[]>{
    const uri = "http://" + this.baseUrl + "/api/chats";
    return this.http.get<HistoryChatDetails[]>(uri);
  }
  getChatById(chatID: number):Observable<Message[]>{
    const uri = "http://" + this.baseUrl + "/api/chat/history/"+chatID;    
    return this.http.get<Message[]>(uri);
  }
  getUserFullNameByUserID(userID: number):Observable<userFullName>{
    const uri = "http://" + this.baseUrl + "/api/users/get_name_by_id/"+userID;
    console.log (uri);
    return this.http.get<userFullName>(uri);
  }
  requestFilteredChats(date:string, branch: string, isCustomRange:boolean):Observable<HistoryChatDetails[]>{
    const uri = `http://${this.baseUrl}/api/chat/history/filter/${date}/${branch}/${isCustomRange}`;
    console.log(uri);
    return this.http.get<HistoryChatDetails[]>(uri);
  }
  requestChatRaiting(chatID:number):Observable<number>{
    const uri = "http://" + this.baseUrl + "/api/chat/getRaiting/"+chatID;
    return this.http.get<number>(uri);
  }
  
}
