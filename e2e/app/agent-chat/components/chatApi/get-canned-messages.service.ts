import { Injectable, NgModule } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';

import { CannedMessage } from './../../shared/models/canned-message-model';

@Injectable()
export class GetCannedMessagesService {

  constructor(private http:HttpClient) { }

  requestAllCannedMessages(agentID:number):Observable<CannedMessage[]>
  {
    const uri = 'http://localhost:62752/api/cannedmessages/' + agentID;
    return this.http.get<CannedMessage[]>(uri);
  }

  requestEditCannedMessage(messageID:number, agentID:number, messageContent: string, messageName:string):Observable<CannedMessage[]>
  {
    messageContent = this.encodingSpecialChars(messageContent);
    messageName = this.encodingSpecialChars(messageName);
    const uri = 'http://localhost:62752/api/cannedmessages/edit/' + agentID+'/'+messageID+'/'+encodeURIComponent(messageName)+'/'+ encodeURIComponent(messageContent);
    return this.http.get<CannedMessage[]>(uri);//check we are using this function write (subscription)
  }

  requestAddCannedMessage(agentID:number, messageName:string, messageContent: string):Observable<CannedMessage[]>
  {
    messageContent = this.encodingSpecialChars(messageContent);
    messageName = this.encodingSpecialChars(messageName);
    const uri = 'http://localhost:62752/api/cannedmessages/add/'+agentID+'/'+messageName+'/'+ messageContent;
    return this.http.get<CannedMessage[]>(uri);
  }

  requestDeleteCannedMessage(agentID:number, messageID:number):Observable<CannedMessage[]>
  {
    const uri = 'http://localhost:62752/api/cannedmessages/delete/'+agentID+'/'+messageID;
    return this.http.get<CannedMessage[]>(uri);
  }

  //Helper functions:
  private encodingSpecialChars(orginalString)
  {
    orginalString = this.replaceAll('.', "***dotEncoding***", orginalString);
    orginalString = this.replaceAll('+', "***plusEncoding***", orginalString);
    orginalString = this.replaceAll("<", "***leftTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll(">", "***rightTringleBracketEncoding***", orginalString);
    orginalString = this.replaceAll("?", "***questionMarkEncoding***", orginalString);
    orginalString = this.replaceAll("/", "***slashEncoding***", orginalString);
    orginalString = this.replaceAll("\\", "***backSlashEncoding***", orginalString);
    orginalString = this.replaceAll("\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("\r\n", "***newLineEncoding***", orginalString);
    orginalString = this.replaceAll("#", "***hashTagEncoding***", orginalString);
    return orginalString;
  }
  private replaceAll(src:string, dst:string, stringToOperateOn:string)
  {
    while(stringToOperateOn.indexOf(src) != -1)
    {
      stringToOperateOn = stringToOperateOn.replace(src, dst);
    }    
    return stringToOperateOn;
  }
}
