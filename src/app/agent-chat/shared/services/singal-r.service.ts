import { Injectable } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';
import { Message } from '../../shared/models/message-model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { GetAgentIdService } from './../../shared/services/get-agent-id.service';
import { GetUserIdService } from './../../shared/services/get-user-id.service';
import { GlobalVariblesService } from './../../shared/services/global-varibles.service';

import { environment } from '../../../../environments/environment';


declare var $: any;


@Injectable()
export class SingalRService {

  public connection: any;
  public proxy: any;
  public url: any;
  public connectionID:string;
  public agentID:number;
  public chosenUserID:number;
  public chatID:number;
  public isReceiverAgent:boolean;
  public latestMessage: Message;
  public observableLatestMessage: BehaviorSubject<Message>;
  public baseUrl:string = environment.baseUrl;
  constructor(private getAgentIdService:GetAgentIdService, private getUserIdService:GetUserIdService, private globalVaribles: GlobalVariblesService) 
  {
    this.latestMessage = null;
    this.observableLatestMessage = new BehaviorSubject<Message>(null);
    this.agentID = this.getAgentIdService.agentID;//shared varible from global singleton service
    this.chosenUserID = this.getUserIdService.userID;//shared varible from global singleton service
    this.startConnection(); //this.globalVaribles.isRegConn
  }

  public startConnection(): void {
      this.connection = $.hubConnection('http://' + this.baseUrl);
      this.proxy = this.connection.createHubProxy('ChatHub');
      this.proxy.on('messageReceived', (a,b,c,d,e,f,g) => this.onMessageReceived(a,b,c,d,e,f,g));//
      this.proxy.on('addNewAWaitingChat', (user) => this.onAddNewAWaitingChat(user));
      this.proxy.on('removeInActiveUser', (userID) => this.onRemoveInActiveUser(userID));
      this.proxy.on('receiveTransferedUser', (user) => this.onReceiveTransferedUser(user));
      this.proxy.on('notifyUserIsDisconnected', (userID) => this.onNotifyUserIsDisconnected(userID));
      this.proxy.on('notifyInactiveUserIsNowConnected', (userID) => this.onNotifyInactiveUserIsNowConnected(userID));
      this.proxy.on('changeUserState', (agentID, state) => this.onChangeUserState(agentID, state));
      
      //notifyUserIsDisconnected
      this.proxy.on('getUserIP', (ip:string, location:string) => this.onGetUserIP(ip, location));
    
  }

  public sendMessage(m: Message): void {
    this.proxy.invoke('Send', m.receiverId, m.senderId, m.content, m.contentLanguage, m.isSenderAgent, m.sessionId, this.connectionID)
       .catch((error: any) => {
           console.log('Send error -> ' + error); 
        });
  }
  public sendMessageToAgents(m: Message): void {
    this.proxy.invoke('SendAgents', m.receiverId, m.senderId, m.content, m.contentLanguage, m.isSenderAgent, m.sessionId, this.connectionID)
       .catch((error: any) => {
           console.log('Send error -> ' + error); 
        });
  }
  public startConn(isAgent) {
    this.isReceiverAgent = isAgent;
    this.globalVaribles.isAgentsChat = isAgent;
    console.log("will call register?" + !this.globalVaribles.isRegConn);
    if(!this.globalVaribles.isRegConn)//will be changed in registerConnection callback.
    {
      this.connection.start().done((data: any) => {
        this.connectionID = data["id"];
          console.log('Connected to Chat Hub');
          this.registerConnection();      
        });
    }
  }
  public registerConnection() {
      this.globalVaribles.chatIDAsObservable.subscribe(chatID=>this.doRegister(chatID));    
  }
  public doRegister(chatID) {
    this.chatID = chatID;
    this.globalVaribles.isRegConn = true;
    if(this.isReceiverAgent)//two agents chat (internal)
    {
      this.proxy.invoke('registerConnID', this.agentID, this.connectionID, true, -1, -1).catch((error: any) => {
        console.log('registerConnID_agents Error error -> ' + error); 
      });
    }
    else//user & agent chat (external)
    {
      this.chosenUserID = this.getUserIdService.userID;
      
      this.proxy.invoke('registerConnID', this.agentID, this.connectionID, true, this.chosenUserID, this.globalVaribles.chatID.value).catch((error: any) => {
        console.log(' registerConnID_user-agent Error error -> ' + error); 
      });
    }
   

  }

  private onMessageReceived( receiverID: number, senderID:number, content:string, language:string,  is_sender_agent:boolean, sessionID: number, time) {
      this.latestMessage = {receiverId:receiverID, senderId:senderID, content:content,
                            contentLanguage:language, isSenderAgent:is_sender_agent,
                             sessionId:sessionID, DateTime:time};
      this.observableLatestMessage.next(this.latestMessage);
      this.globalVaribles.pushNewNotificationToHandle(senderID, is_sender_agent);    
  }


  public onAddNewAWaitingChat(user:any) {
    this.globalVaribles.pushNewNotificationToHandle(user.userID, false);    
    this.globalVaribles.newUser.next(user);
  }

  public transferUser(destAgnetID:number, UserIDToTransfer:number) {//called when we are transfferring a user 
    
    this.proxy.invoke('transferUser', destAgnetID, UserIDToTransfer, this.globalVaribles.chatID.value).catch((error: any) => {
      console.log('transferUser error -> ' + error); 
    });
    this.globalVaribles.weJustTransferedAUser.next(UserIDToTransfer);
    
  }
  public onReceiveTransferedUser(user:any) {//called when a user is transfered to us
    this.globalVaribles.newUser.next(user);
    this.globalVaribles.gotTransferedUser.next(user.userID);
    this.globalVaribles.pushNewNotificationToHandle(user.userID, false);        
  }
  public onNotifyUserIsDisconnected(uid:number) {
    this.globalVaribles.userInActive.next(uid);
  }
  public onRemoveInActiveUser(uid:number) {
    this.globalVaribles.shouldRemoveInActiveUser.next(uid);
  }
  public onNotifyInactiveUserIsNowConnected(uid:number) {
    this.globalVaribles.userInActiveWasReconnected.next(uid);    
  }
  public onChangeUserState(agentID, state) {
    this.globalVaribles.agentState.next(state);    
    this.globalVaribles.agentIDToChangeState.next(agentID);
  }
  public updateCurrAgent(agentID:number, state:string) {//this function returning message history AND creating a new chat in chat table. 
    this.proxy.invoke('changeAgentState', agentID, state).catch((error: any) => {
      console.log('changeAgentState error -> ' + error); 
    });
  }
  public onGetUserIP(ip:string, location:string) {
    this.globalVaribles.updateIpUserIDAndLocation(ip, location, this.chosenUserID);
  }  

}
