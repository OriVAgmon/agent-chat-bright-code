import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';  


@Injectable()
export class TranslateService {

  constructor(private http: Http) { }
  translatedText;
  translate(stringToTranslate)
  {
    let uri = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=en&dt=t&q="+encodeURI(stringToTranslate);
    return this.http.get(uri)  
               .map((res: Response) => {return res.json()  
    });  
  }
   
}
